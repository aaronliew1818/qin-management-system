import express from 'express';
import bodyParser from 'body-parser';
import uuidv4 from 'uuid/v4';
import cors from 'cors';
import session from 'express-session';
import trimRequest from './app/middleware/trimRequest';
import config from './config';
import { router } from "./routes";
import { notFound } from './utils/errors';

// run cron job
// require('./cron/cProtectExpiredReminder'); IN0013, to be remove later

main();

async function main() {
  const app = express();
  const admin = require("firebase-admin");
  admin.initializeApp({
    credential: admin.credential.applicationDefault(),
  });

  app.use(cors({
    origin: function (origin, callback) {
      return callback(null, true);
    },
    optionsSuccessStatus: 200,
    credentials: true
  }));
  app.set('trust proxy', true);
  // app.use(cors()); //TODO : set whitelist to certain api
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(trimRequest);
  app.use('/services/qms', router);
  app.get('/health-check', async (req, res) => res.json('Alive!'));
  app.use(notFound);

  app.listen(config.server.port, () => {
    console.log(`Aspirasi insurance app listening on port ${config.server.port}`);
  })
}
