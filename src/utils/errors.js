/*
  Catch Errors Handler
  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch and errors they throw, and pass it along to our express middleware with next()
*/

export const catchErrors = (fn) => {
  return function (req, res, next) {
    return fn(req, res, next).catch(next);
  };
};

export const notFound = (req, res, next) => {
  return res.status(404).json({ message: 'Not Found' })
};

export const unprocessableEntity = (err, req, res, next) => {
  next(res.status(422).json({ message: err }))
};

export const forbidden = (req, res, next) => {
  next(res.status(403).json({ message: 'Forbidden' }))
};

export const unauthorized = (req, res, next) => {
  next(res.status(401).json({ message: 'Unauthorized' }))
};

