import config from '../config'

export default {
  'CountryCode': config.ge.countryCode,
  'ClientOrgCode': config.ge.clientOrgCode,
  'ProviderOrgCode': config.ge.providerOrgCode,
  // 'TxnRefNumber': '4-AXIATA-20191123225700',
  'Content-Type': 'application/json',
  'Origin': config.ge.origin,
  'KeyId': config.ge.keyId,
}