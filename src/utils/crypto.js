import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import config from '../config'
import fs from 'fs'

const path = require("path");
const privateKey = fs.readFileSync(path.resolve(__dirname, "../qin.key"), 'utf8');
const publicKey = fs.readFileSync(path.resolve(__dirname, "../qin.key.pub"), 'utf8');

const generateAuthToken = (username, role) => {
  // On sign
  // return jwt.sign(
  //     {
  //       username,
  //       role
  //     },
  //     config.server.jwtSecrect,
  //     {
  //       expiresIn: '1d' // default is seconds (1h, 7d, 1second)
  //     }
  // )

  return jwt.sign(
    {
      username,
      role
    },
      privateKey,
    {
      algorithm: 'RS256',
      expiresIn: '1d' // default is seconds (1h, 7d, 1second)
    }
  )
}

const decodeAuthToken = (request) => {
  if (request.headers.authorization) {
    const token = request.headers.authorization.replace('Bearer ', '')
    return jwt.verify(token, publicKey,{ algorithm: 'RS256' }, (err, user) => {
      if (err) {
        return null;
      }
      return user;
    });
  }

  return null
}

const encryptPaymentToken = (value) => {
  let cipher = crypto.createCipheriv('aes-256-cbc', config.server.paymentKeySecrect, config.server.paymentIVSecrect);
  let encrypted = cipher.update(value, 'utf8', 'hex');
  encrypted += cipher.final('hex');
  return encrypted
}

const decryptPaymentToken = (value) => {
  let decipher = crypto.createDecipheriv('aes-256-cbc', config.server.paymentKeySecrect, config.server.paymentIVSecrect);
  let decrypted = decipher.update(value, 'hex', 'utf8');
  decrypted += decipher.final('utf8');
  return decrypted;
}


const generateHMAC = async (jsonRequestData, timestamp) => {
  let jsonRequestString = JSON.stringify(jsonRequestData);
  jsonRequestString = jsonRequestString.replace(/\s+/g, ''); //replace space
  jsonRequestString = jsonRequestString.replace(/\r?\n|\r/g, ''); //replace newline

  const sha256 = crypto.createHash("sha256");
  sha256.update(jsonRequestString, "ascii");//utf8 here
  let base64_hased_json_request = sha256.digest("base64");
  let concatenated_string_to_hash = "POST" + ":" + "/my/general/v1/products/travel/proposal" + ":" + timestamp + ":" + base64_hased_json_request;
  let hmac_base64_hash = crypto.createHmac('sha256', config.ge.secretKey).update(concatenated_string_to_hash).digest('base64');
  return hmac_base64_hash;
}

const generateApiSecretKey = async (payload) => {  
  const apiHmac = crypto.createHmac('sha256', config.server.internalAPISecretKey).update(payload).digest('hex');
  return apiHmac;
}

export {
  generateAuthToken,
  decodeAuthToken,
  encryptPaymentToken,
  decryptPaymentToken,
  generateHMAC,
  generateApiSecretKey
}
