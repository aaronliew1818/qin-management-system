import uniqid from 'uniqid';
import _ from 'lodash';
import { isApplicationIdExist } from '../services/db';
import admin from "firebase-admin";
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();


export const isRequired = (name) => { throw new Error(`${name} is required`); };

export const toCent = (amount) => amount * 100;

export const toDollar = (amount) => amount / 100;

export const calTotalAmount = (geAmount, fees) => geAmount + fees;

export const toDollarDisplay = (amount, changeToDollar, style, showDecimal) => {
  const dollar = changeToDollar ? toDollar(amount) : amount;
  return new Intl.NumberFormat("en-MY", {
    style: style ? style : "currency", //currency or decimal
    currency: 'MYR',
    minimumFractionDigits: showDecimal ? 2 : 0,
  }).format(dollar);
}

export const timer = (ms = isRequired('millisecond')) => new Promise(res => setTimeout(res, ms));

export const generateUniqueId = async (prefix = isRequired('prefix')) => {
  prefix = prefix.toUpperCase();
  let uniqueId = "";
  const prefixType = {
    T: {
      tableName: 'application_tab',
      id: 'at_id'
    },
    L: {
      tableName: 'life_application_tab',
      id: 'lat_id'
    },
    HC: {
      tableName: 'hospi_application_tab',
      id: 'hat_id'
    },
    SPX: {
      tableName: 'sme_protectx_application_tab',
      id: 'spat_id'
    },
    CP: {
      tableName: 'c_protect_application_tab',
      id: 'cpat_id'
    },
    BP: {
      tableName: 'bill_protect_application_tab',
      id: 'bpat_id'
    },
    ACP: {
      tableName: 'card_protect_application_tab',
      id: 'cpat_id'
    },
    S6: {
      tableName: 'protect_super6_application_tab',
      id: 'psat_id'
    },
    SP3: {
      tableName: 'sme_protect_3in1_application_tab',
      id: 'sp3at_id'
    },
    SP: {
      tableName: 'sme_protect_application_tab',
      id: 'spat_id'
    },
  }

  if (_.isEmpty(prefixType[prefix])) throw new Error(`prefix type object not found`);

  do { //generate unique id 
    uniqueId = uniqid.process(`${prefix}-`);
  } while (await isApplicationIdExist(prefixType[prefix].tableName, prefixType[prefix].id, uniqueId) || uniqueId.length > 20);

  return uniqueId;
};

const zeroPad = (num, places) => String(num).padStart(places, '0');

export const generateEcertId = (prefix = isRequired('prefix'), value = isRequired('value')) => {
  prefix = prefix.toUpperCase();
  const prefixType = {
    L: {
      type: 'AX100',
    },
    HC: {
      type: 'AX200'
    },
    ACP: {
      type: 'AX300'
    },
    S6:{
      type: 'AX500'
    }
  }

  if (_.isEmpty(prefixType[prefix])) throw new Error(`prefix type object not found`);
  // return `${prefixType[prefix].type} ${'00000000' + value}`
  return `${prefixType[prefix].type} ${zeroPad(value, 9)}`
};

export const generateTakafulEcertId = (prefix = isRequired('prefix'), value = isRequired('value')) => {
  prefix = prefix.toUpperCase();
  const prefixType = {
    SP3: {
      type: 'AF002'
    },
  }

  if (_.isEmpty(prefixType[prefix])) throw new Error(`prefix type object not found`);
  return `${prefixType[prefix].type} ${zeroPad(value, 6)}`
};

export const addPrefixInsuredPersonListData = (data) => {

  if (!_.isEmpty(data.insuredPersonList)){

    const newInsuredPersonList = data.insuredPersonList.map((obj, index) => ({ 
      seqNo: index + 1,
      ...obj,
      customerType: '',
      insSalutation: (obj.insSalutation) ? obj.insSalutation : 'Mr',
      insGivenName: '',
      insGender: 'Unknown',
      insNationality: 'MY',
      insFinnum: '',//default ''
      insItemRelationship: (index + 1 === 1) ? 'Main Insured' : 'Friend',
      insMailingAddr1: 'Level 29 Axiata Tower, 9 Jalan Stesen Sentral 5',//harcode //address follow main insured
      insMailingAddr2: '',
      insMailingAddr3: '',
      insMailingAddr4: '',
      insCountry: 'KUALA LUMPUR',//harcode
      insPostalCode: '50470',//harcode
      insPassportNo: 'A999999',//hardcode
      insMaritalStatus: 'Single',//hardcode
      insSameAddressProposer: 'YES',//hardcode
      insInfo: '',
      insNominees: []
    }))

    data.insuredPersonList = newInsuredPersonList    
  } 
  return data;
}

export const convertGeErrosMessages = (data) => {
  if (!_.isEmpty(data) && !_.isEmpty(data.errors)) {
    let message = [];
    for (let errorType in data.errors) {
      message.push({ [errorType]: data.errors[errorType]['errorMessage'] });
    }
    return message;
  }
  return null;
}

export const getInternationalNumber = (phoneNumber, country = 'MY') => {
  try {
    const number = phoneUtil.parseAndKeepRawInput(phoneNumber, country);
    // return `${number.getCountryCode()}${number.getNationalNumber()}`;
    return `0${number.getNationalNumber()}`;
  }catch (err) {
    return phoneNumber;
  }
}

export const postNotification = (title, content, topic = "tuition") => {

  const payload = {
    notification: {
      title: title,
      body: content,
      sound : "default"
    }
  };

  // Set the message as high priority and have it expire after 24 hours.
  const options = {
    priority: 'high',
    timeToLive: 60 * 60 * 24
  };

  // Send a message to devices subscribed to the provided topic.
  admin.messaging().sendToTopic(topic, payload, options)
      .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });
}
