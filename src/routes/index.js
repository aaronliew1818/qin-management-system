import express from 'express';
import { appController } from '../app/controllers/appController';
import { authController } from '../app/controllers/authController';
import { registrationController } from '../app/controllers/registrationController';
import { paymentController } from '../app/controllers/paymentController';
import validateRequest from '../app/middleware/validateRequest';
import {
    loginSchema,
    studentInfoSchema,
    updateStudentInfoSchema,
    updateStudentStatusSchema,
    studentPaymentSchema,
    studentMiscPaymentSchema,
    teacherPaymentSchema,
    teacherInfoSchema,
    updateTeacherInfoSchema,
    cancelTuitionSchema,
    courseInfoSchema,
    courseSubcriptionSchema,
    cancelSubcriptionSchema,
    createAnnouncementSchema,
    updateAnnouncementSchema,
    createQuizSchema,
    updateQuizSchema,
    createNoteSchema, updateNoteSchema, updateTimetableSchema, createTimetableSchema
} from '../app/schema';
import auth from '../app/middleware/auth';
import requestJWTLimiter from '../app/middleware/requestJWTLimiter';

const router = express.Router();
router.get('/healthcheck', appController.getHeathCheck);
router.post('/login', validateRequest(loginSchema), authController.login);

router.get('/classroom', appController.getClassroom);


router.post('/announcements/create', auth, validateRequest(createAnnouncementSchema), appController.createAnnouncement);
router.post('/announcements/update', auth, validateRequest(updateAnnouncementSchema), appController.updateAnnouncement);
router.get('/announcements', auth, appController.getAnnouncements)

router.post('/quiz', auth, validateRequest(createQuizSchema), appController.createQuiz);
router.put('/quiz', auth, validateRequest(updateQuizSchema), appController.updateQuiz);
router.get('/quiz', auth, appController.getQuiz)

router.post('/note', auth, validateRequest(createNoteSchema), appController.createNote);
router.put('/note', auth, validateRequest(updateNoteSchema), appController.updateNote);
router.get('/note', auth, appController.getNotes)


router.get('/students', auth, appController.getAllStudents);
router.get('/students/enrollment', appController.getNewSubjectEnrollmentStudents);
router.get('/student', auth, appController.getSingleStudentByUuid);
router.post('/student/register', auth, validateRequest(studentInfoSchema), registrationController.registerStudent);
router.post('/student/update', auth, validateRequest(updateStudentInfoSchema), registrationController.updateStudentInfo);
router.post('/student/status', auth, validateRequest(updateStudentStatusSchema), registrationController.updateStudentStatus);
router.post('/student/course/cancel', auth, validateRequest(cancelSubcriptionSchema), registrationController.cancelCourseSubscriptionById);
router.post('/student/misc/cancel', auth, validateRequest(cancelSubcriptionSchema), registrationController.cancelMiscSubscriptionById);
router.get('/students/payment', auth, paymentController.getStudentPaymentByMonthNameAndProgramLvl);
router.get('/students/misc/payment', auth, paymentController.getStudentMiscPaymentByMonthNameAndProgramLvl);
// router.get('/student/payment', paymentController.getSingleStudentPaymentDetail);
router.get('/student/payment', auth, paymentController.getStudentPaymentDetail);
router.get('/student/misc/payment', auth, paymentController.getStudentMiscPaymentDetail);
router.post('/student/payment', auth, validateRequest(studentPaymentSchema), paymentController.payStudentFees);
router.post('/student/misc/payment', auth, validateRequest(studentMiscPaymentSchema), paymentController.payStudentMiscFees);


router.get('/teachers', auth, appController.getAllTeachers);
router.get('/teachers/salary', auth, paymentController.getTeachersSalary);
router.get('/teacher/salary', auth, paymentController.getTeacherSalaryDetail);
router.post('/teacher/salary', auth, validateRequest(teacherPaymentSchema), paymentController.payTeacherSalary);
router.get('/teacher', auth, appController.getSingleTeacherByUuid);
router.post('/teacher/register', auth, validateRequest(teacherInfoSchema), registrationController.registerTeacher);
router.post('/teacher/update', auth, validateRequest(updateTeacherInfoSchema), registrationController.updateTeacherInfo);
router.post('/teacher/tuition/cancel', auth, validateRequest(cancelTuitionSchema), registrationController.cancelTuitionFeeById);

router.get('/tuitionFees', auth, appController.getAllTuitionFees);
router.get('/misc', auth, appController.getAllMisc);

router.post('/course/register', auth, validateRequest(courseInfoSchema), registrationController.registerCourse);
// router.post('/course/subscribe', validateRequest(courseSubcriptionSchema), registrationController.subscribeStudentCourse);

router.get('/courses', auth, appController.getAllCourses);
router.get('/programLvl', auth, appController.getProgramLvl);

router.get('/banks', auth, paymentController.getListOfBanks);

router.get('/payment/history', auth, paymentController.getPaymentHistory);

router.get('/finance/report/students', paymentController.getStudentsFinanceReport);
router.get('/finance/report/month', auth, paymentController.getMonthlyFinanceReport);

router.get('/student/timetables', auth, appController.getTimetables);
router.put('/student/timetables', auth, validateRequest(updateTimetableSchema), appController.updateTimetable);
router.post('/student/timetables', auth, validateRequest(createTimetableSchema), appController.createTimetable);

// router.post('/auto-purchase-hospicash', validateRequest(saveHospicashSchema), appController.autoPurchaseHospicash);
// router.post('/send-cportect-charging-sms', validateRequest(sendCPortectChargingSMSSchema), appController.sendCPortectChargingSMS);

// router.post('/get-premium', auth, validateRequest(getPremiumSchema), appController.getPremium);
export { router };

