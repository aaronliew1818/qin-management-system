import { IncomingWebhook } from '@slack/webhook';
import config from '../config';
import { isRequired} from '../utils'

const errorChannelWebHook = new IncomingWebhook(config.slack.errorWebhookUrl);

const sendErrorToSlackChannel = async ({ type = isRequired('type'), triggerAt = isRequired('triggerAt'), sid = '', message = isRequired('message')}) => {
  try {
    await errorChannelWebHook.send({
      blocks: [
        {
          type: "section",
          text: { type: "mrkdwn", text: `*${type} Error*` }
        },
        {
          type: "section",
          fields: [
            { type: "mrkdwn", text: `Trigger at:\n${triggerAt}` },
            { type: "mrkdwn", text: `Session id:\n${sid}` }
          ]
        },
        { type: "section", text: { type: "mrkdwn", text: `Message:\n${message}` } }
      ]
    });
  } catch (err) {
    throw err;
  }
}

export {
  sendErrorToSlackChannel
}