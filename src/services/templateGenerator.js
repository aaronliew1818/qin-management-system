import fs from 'fs';
import path from 'path';
// import puppeteer from 'puppeteer';
import handlebars from 'handlebars';

// export const generatePDFBase64 = async (templateFile, data) => {
//     const templateHtml = fs.readFileSync(path.join(`${__dirname}/../template/pdf`, `${templateFile}.html`), 'utf8');    
//     const template = handlebars.compile(templateHtml);
//     const html = template(data);
//     const browser = await puppeteer.launch({ args: ['--no-sandbox'], headless: true });
//     const page = await browser.newPage();
//     await page.goto(`data:text/html;charset=UTF-8,${html}`, { waitUntil: 'networkidle0'});
//     const pdf = await page.pdf({
//         format: 'A4'
//     });
//     await browser.close();
//     return pdf.toString('base64')
// }

export const generateEmailContent = async (templateFile, data) => {    
    const templateHtml = fs.readFileSync(path.join(`${__dirname}/../../template/email`, `${templateFile}.html`), 'utf8');    
    const template = handlebars.compile(templateHtml);
    const html = template(data);
    return html
}


