import axios from 'axios';
import config from '../config';
import { isRequired } from '../utils';
import { generateEmailContent } from '../services/templateGenerator';

// sample attachments 
// [{
//     "type": "application/pdf",
//     "fileName": "sample.pdf",
//     "contentId": "Sample attachment",
//     "content": "base64 string"
// }]
export const sendEmail = async ({ subject = isRequired('subject'), content = isRequired('content'), emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [] }) => {
    return axios.post(
        `${config.lms.emailUrl}`,
        { secretKey: config.lms.emailSecretKey, subject, content, emailTo, replyTo, attachments, identifier }
    )
    .then(() => true)
    .catch((error) => { 
        return false;
    });
}

export const sendConfirmationEmailToLifeApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [] , meta = {} }) => {

    const content = await generateEmailContent('enrollment-confirmation', {
        name: meta.name,
        ecertNumber: meta.ecertNumber,
        fromPolicyEffectiveDate: meta.fromPolicyEffectiveDate,
        toPolicyEffectiveDate: meta.toPolicyEffectiveDate, // fromPolicyEffectiveDate + 7 days 
    })
    const result = await sendEmail({
        subject: 'Enrollment Confirmation Email',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}


export const sendConfirmationEmailToHospiCashApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('hospicash-enrollment-confirmation', {
        name: meta.name,
        ecertNumber: meta.ecertNumber,
        fromPolicyEffectiveDate: meta.fromPolicyEffectiveDate,
        toPolicyEffectiveDate: meta.toPolicyEffectiveDate, // fromPolicyEffectiveDate + 6 months
    })
    const result = await sendEmail({
        subject: 'Enrollment Confirmation Email',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}


export const sendEmailToSMEProtectXApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('smeprotectx-enrollment', {
        name: meta.name,
    })
    
    const result = await sendEmail({
        subject: 'You are now covered by Aspirasi SME OwnerProtect-X!',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}


export const sendEmailToCProtectApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('cprotect-enrollment', {
        name: meta.name,
    })

    const result = await sendEmail({
        subject: 'You are now covered by Aspirasi C-Protect',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}

export const sendEmailToBillProtectApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('bill-protect-enrollment', {
        name: meta.name,
    })

    const result = await sendEmail({
        subject: 'You are now covered by Aspirasi BillProtect',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}

export const sendEmailToCardProtectApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('card-protect-enrollment', {
        name: meta.name,
        ecertNumber: meta.ecertNumber,
        fromPolicyEffectiveDate: meta.fromPolicyEffectiveDate,
        toPolicyEffectiveDate: meta.toPolicyEffectiveDate, // fromPolicyEffectiveDate + 7 days 
    })
    const result = await sendEmail({
        subject: 'Enrollment Confirmation Email',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}


export const sendEmailToProtectSuper6ApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('protect-super6-enrollment', {
        name: meta.name,
        ecertNumber: meta.ecertNumber,
        fromPolicyEffectiveDate: meta.fromPolicyEffectiveDate,
        toPolicyEffectiveDate: meta.toPolicyEffectiveDate, 
    })
    const result = await sendEmail({
        subject: 'Enrollment Confirmation Email',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}

export const sendEmailToSMEProtect3In1ApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('smeprotect-3in1-confirmation', {
        name: meta.name,
        ecertNumber: meta.ecertNumber,
        fromPolicyEffectiveDate: meta.fromPolicyEffectiveDate,
        toPolicyEffectiveDate: meta.toPolicyEffectiveDate,
    })
    const result = await sendEmail({
        subject: 'Enrollment Confirmation Email',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}

export const sendEmailToSMEOwnerProtectApplicationUser = async ({ emailTo = isRequired('emailTo'), replyTo = isRequired('replyTo'), identifier = isRequired('identifier'), attachments = [], meta = {} }) => {

    const content = await generateEmailContent('smeprotect-enrollment', {
        name: meta.name,
        fromPolicyEffectiveDate: meta.fromPolicyEffectiveDate,
        toPolicyEffectiveDate: meta.toPolicyEffectiveDate,
    })

    const result = await sendEmail({
        subject: 'You are now covered by Aspirasi SME OwnerProtect!',
        content,
        emailTo,
        replyTo,
        identifier,
        attachments
    })

    return result;
}
