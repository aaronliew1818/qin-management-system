import mariadb from 'mariadb';
import moment from 'moment';
import session from 'express-session'
import _ from 'lodash';
import config from '../config'

const MySQLStore = require('express-mysql-session')(session);

const pool = mariadb.createPool({
  host: config.db.host,
  port: config.db.port,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database
});

export const sessionStore = new MySQLStore({
  host: config.db.host,
  port: config.db.port,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  clearExpired: true,
  schema: {
    tableName: 'sessions_tab',
    columnNames: {
      session_id: 'st_id',
      expires: 'st_expires',
      data: 'st_data'
    }
  }
});

export const queryOrUpdateDB = async(q) =>{
  let conn;
  try {
    conn = await pool.getConnection();
    const data = await conn.query(q);
    delete data['meta'] // remove unsure info
    if (data.length <= 1){ return _.isEmpty(data) ? null : data[0] }
    return data;
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
}

export const insertDB = async (q,v) => {
  let conn;
  try {
    conn = await pool.getConnection();
    const data = await conn.query(q, v);
    return data;
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
}

export const isApplicationIdExist = async (tableName, field, id) => {
  let conn;
  try {
    conn = await pool.getConnection();
    let querySQL = `select ${field} from ${tableName} where ${field}="${id}"`;
    const data = await conn.query(querySQL);
    delete data['meta'] // remove unsure info
    if (!_.isEmpty(data)) return true;
    else return false;
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
}

export const insertGEResponse = async ({ url, method, productType, statusCode, requestData = '', result = '', message, st_id, user_st_id }) => {
  let conn;
  try {
    conn = await pool.getConnection();

    const insertSQL = `INSERT INTO ge_response 
    ( ger_url, ger_method, ger_product_type, ger_record_create_date, ger_request_data, ger_status_code, ger_result, ger_message, ger_session_id, ger_user_session_id)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ? )`;

    await conn.query(insertSQL, [
      url,
      method,
      productType,
      moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
      requestData,
      statusCode,
      result,
      message,
      st_id,
      user_st_id
    ]);

  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
}

export const insertValidationError = async ({ request, response, url, sessionId, productType}) => {

  let conn;
  try {
    conn = await pool.getConnection();
    const insertSQL = `INSERT INTO application_validation_error_tab 
    ( avet_request_data, avet_response_data, avet_url, avet_session_id, avet_product_type, avet_record_create_date)
    VALUES (?, ?, ?, ?, ?, ? )`;

    await conn.query(insertSQL, [
      request,
      response,
      url,
      sessionId,
      productType,
      moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
    ]);

  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
}

export const getCProtectPaymentStatusWithMsisdn = async (phoneNumber) => {
  let conn;
  try {
    conn = await pool.getConnection();
    let querySQL = `select cpat_status from c_protect_application_tab where cpat_mobile_phone_number = '${phoneNumber}' and cpat_status = '1'`;
    const data = await conn.query(querySQL);
    delete data['meta'] // remove unsure info
    if (!_.isEmpty(data)) return 2; // subscribe
    else return 1; // resubscribe
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.release(); //release to pool
  }
}





