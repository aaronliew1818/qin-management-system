import { resolve } from 'path';
import { config as configDotenv } from 'dotenv';

const nodeEnv = process.env.NODE_ENV || 'development';

configDotenv({
  path: resolve(process.cwd(), `.env.${nodeEnv}`),
});

const isDevEnv = nodeEnv === 'development';

const config = {
  server: {
    port: parseInt(process.env.PORT) || 8080,
    isDevEnv,
    jwtSecrect: process.env.JWT_SECRECT,
    sessionSecrect: process.env.SESSION_SECRECT,
    paymentKeySecrect: process.env.PAYMENT_KEY_SECRECT,
    paymentIVSecrect: process.env.PAYMENT_IV_SECRECT,
    internalAPISecretKey: process.env.INTERNAL_API_SECRET_KEY,
    internalMiddlewareUrl: process.env.INTERNAL_MIDDLEWARE_URL
  },
  db: {
    host: process.env.MARIADB_HOST,
    port: process.env.MARIADB_PORT,
    user: process.env.MARIADB_USER,
    password: process.env.MARIADB_PASSWORD,
    database: process.env.MARIADB_DATABASE,
  },
  slack:{
    errorWebhookUrl: process.env.SLACK_ERROR_WEBHOOK_URL,
  },
  lms:{
    emailUrl: process.env.LMS_EMAIL_URL,
    emailSecretKey: process.env.LMS_EMAIL_SECRET_KEY
  },
  sftp:{
    host: process.env.SFTP_HOST,
    user: process.env.SFTP_USER,
    password: process.env.SFTP_PASSWORD,
  },
  ge:{
    api: process.env.GE_API,
    keyId: process.env.GE_KEY_ID,
    secretKey: process.env.GE_SECRET_KEY,
    countryCode: 'MY',
    clientOrgCode: process.env.GE_CLIENT_ORG_CODE,
    providerOrgCode: process.env.GE_PROVIDER_ORG_CODE,
    origin: process.env.GE_ORIGIN,
    param : {      
      channelId: process.env.GE_CHANNEL_ID,
      channelCode: 'ADC', 
      companyId: process.env.GE_COMPANY_ID,
      agentId: process.env.GE_AGENT_ID,
      agentCode: null,
      planName: 'Plan 1',	
      addBenefitIds	: null,
      addBenefitCodes: null,
      campaignCode: '',
      tripCost: '1.0',
      ccNumber: null,
      areaTravel: 'A2', 
    },
    travel:{
      minAge: 2,
      maxAge: 70,
      int: { // international
        productId: process.env.GE_INT_PRODUCT_ID,
        productCode: 'AMY', // Aspirasi Travel Worldwide
        planId: process.env.GE_INT_PLAN_ID,
        planCode: 'AMY1',
      },
      dom: { // domestic
        productId: process.env.GE_DOM_PRODUCT_ID,
        productCode: 'AMD', // Aspirasi Travel Local
        planId: process.env.GE_DOM_PLAN_ID,
        planCode: 'AMD1',
      },
    },
    lifeInsure: {
      minAge: 18,
      maxAge: 69,
      effectiveDuration: 6, // days
      premium: 150,
      fees: 50,
      tax: 0,
      faq: require('./pdf/lifeInsure/faq'),
      policyContractBase64PDF: require('./pdf/lifeInsure/groupMasterPolicy'),
      pdsBase64PDF: require('./pdf/lifeInsure/pds'),
    },
    hospiCash: {
      minAge: 18,
      maxAge: 50,
      maxRenewableAge: 55,
      premium: 6000,
      fees: 120,
      tax: 0,      
      effectiveDuration: 6, // months
      faq: require('./pdf/hospicash/faq'),
      pds: require('./pdf/hospicash/pds'),
      groupMasterPolicy: require('./pdf/hospicash/groupMasterPolicy'),
    },
    smeProtectX: {
      faq: require('./pdf/smeProtectX/faq'),
      pds: require('./pdf/smeProtectX/pds'),
      groupMasterPolicy: require('./pdf/smeProtectX/groupMasterPolicy'),
    },
    cProtect:{
      minAge: 16,
      maxAge: 79,
      premium: 100,
      fees: 0,
      tax: 0,
      faq: require('./pdf/cProtect/faq'),
      pds: require('./pdf/cProtect/pds'),
      groupMasterPolicy: require('./pdf/cProtect/groupMasterPolicy'),
    },
    billProtect: {
      minAge: 16,
      maxAge: 80,
      premium: 3000,
      fees: 50,
      tax: 180,
      effectiveDuration: 6, // months
      faq: require('./pdf/billProtect/faq'),
      pds: require('./pdf/billProtect/pds'),
      groupMasterPolicy: require('./pdf/billProtect/groupMasterPolicy'),
    },
    cardProtect:{
      minAge: 18,
      maxAge: 59,
      plan :{
        ACP005:{
          name: 'Starting Out',
          code: 'ACP005',
          premium: 1000,
          fees: 50,
          tax: 0,
        },
        ACP010:{
          name: 'Go-Getter',
          code: 'ACP010',
          premium: 2000,
          fees: 50,
          tax: 0,
        },
        ACP015:{
          name: 'Rising Star',
          code: 'ACP015',
          premium: 3000,
          fees: 50,
          tax: 0,
        },
        ACP025:{
          name: 'A-Lister',
          code: 'ACP025',
          premium: 5000,
          fees: 100,
          tax: 0,
        }
      },
      effectiveDuration: 6, // months
      faq: require('./pdf/cardProtect/faq'),
      pds: require('./pdf/cardProtect/pds'),
      groupMasterPolicy: require('./pdf/cardProtect/groupMasterPolicy'),
    },
    protectSuper6: {
      premium: 3900,
      fees: 50,
      tax: 0,
      minAge: 18,
      maxAge: 69,
      effectiveDuration: 6, // months
      faq: require('./pdf/protectSuper6/faq'),
      pds: require('./pdf/protectSuper6/pds'),
      groupMasterPolicy: require('./pdf/protectSuper6/groupMasterPolicy'),
    },
    smeProtect3In1: {
      minAge: 18,
      maxAge: 50,
      maxRenewableAge: 55,
      effectiveDuration: 6, // months
      plan: {
        ASOP20: {
          name: 'Starting Out',
          code: 'ASOP20',
          premium: 12000,
          fees: 240,
          tax: 0,
        },
        ASOP30: {
          name: 'Go-Getter',
          code: 'ASOP30',
          premium: 14500,
          fees: 290,
          tax: 0,
        }
      },
      faq: require('./pdf/smeProtect3In1/faq'),
      pds: require('./pdf/smeProtect3In1/pds'),
      masterCertificate: require('./pdf/smeProtect3In1/masterCertificate'),
    },
    smeProtect: {
      premium: 4500,
      fees: 95, 
      tax: 270, 
      minAge: 18,
      maxAge: 65,
      effectiveDuration: 12, // months
      faq: require('./pdf/smeProtect/faq'),
      pds: require('./pdf/smeProtect/pds'),
      groupMasterPolicy: require('./pdf/smeProtect/groupMasterPolicy'),
    },
  }
};

export default config;