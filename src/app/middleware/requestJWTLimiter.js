import rateLimit from 'express-rate-limit'

const requestJWTLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour window
  max: 5, // start blocking after 5 requests
  message:
    { error: "Too many request from this IP, please try again after an hour"}
});

export default requestJWTLimiter;