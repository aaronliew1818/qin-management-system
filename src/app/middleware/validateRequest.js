import { insertValidationError } from '../../services/db';
 
const validateRequest = (schema, productType = 'Unknown') => async (req, res, next) => {
  
  try {
    await schema.validate(req.body, { abortEarly: false });
    next();
  } catch (e) {

    console.log(e);
    const message = e.inner.reduce(function (accumulator, current, i) {
      accumulator[current.path] = current.message;
      return accumulator;
    }, {});

    // insertValidationError({
    //   request: req.body,
    //   response: message,
    //   url: `${req.protocol}://${req.get('host')}${req.originalUrl}`,
    //   sessionId: req.session.id,
    //   productType
    // });
      
    return res.status(422).json({ error: { message} })    
  }
};

export default validateRequest;

