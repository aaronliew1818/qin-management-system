import { decodeAuthToken } from '../../utils/crypto'

import { authDao } from '../dao/authDao'

const auth = (req, res, next) => {
  try{
    const user = decodeAuthToken(req, res, next);
    if (!user) { return res.status(404).json({ message: "Invalid Token" })}
    const isValidUser = authDao.validateUsernameAndRole(user.username, user.role);
    if (isValidUser == null){
      return res.status(401).json({ message: "Unauthorized" })
    }
    next()

  }catch(e){
    return res.status(404).json({ message: e.message })
  }
}

export default auth;