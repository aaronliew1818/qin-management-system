const trimStringProperties = (obj) => {

  if (obj !== null && typeof obj === 'object') {

    for (var prop in obj) {

      // if the property is an object trim it too
      if (typeof obj[prop] === 'object') {
        return trimStringProperties(obj[prop]);
      }

      // if it's a string remove begin and end whitespaces
      if (typeof obj[prop] === 'string') {
        obj[prop] = obj[prop].trim();
      }
    }
  }
}

const trimRequest = (req, res, next) => {

  if (req.body) {
    if (req.path !== '/ge/save-smeownerprotect-x'){
      trimStringProperties(req.body);
    }
  }
  next();
}

export default trimRequest;