import {insertDB, queryOrUpdateDB} from "../../services/db";


function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

export const notesDao = {

    getAllNotes: async() => {
        // "at_id": 104,
        //     "at_author_name": "Admin",
        //     "at_author_id": null,
        //     "at_content": "一二三四五",
        //     "at_images": null,
        //     "at_status": "ACTIVE",
        //     "at_created_date": "2022-08-10T23:47:44.000Z",
        //     "at_updated_date": "2022-08-10T23:47:44.000Z"
        // snt_id,snt_tuition_id,snt_url,snt_status,snt_record_create_date,snt_record_update_date
        const notes = await queryOrUpdateDB(`
            select snt_id as id,
                   snt_tuition_id as tuitionId,
                   snt_title as title,
                   snt_url as url,
                   snt_status as status,
                   snt_record_create_date as recordCreateDate,
                   snt_record_update_date as recordUpdateDate 
            from student_note_tab where snt_status = 'ACTIVE' order by snt_record_create_date desc
          `);
        if (notes == null) {
            return [];
        } else if (!isArray(notes)){
            return [notes]
        }
        return notes;
    },

    createNote: async(note) => {
        return await insertDB(`INSERT INTO student_note_tab
                                   (snt_tuition_id,snt_title, snt_url,snt_status,snt_record_create_date,snt_record_update_date)
                               VALUES (?, ?, ?, ?, now(), now())`, note);
    },

    updateNoteById: async({noteId, tuitionId, title, url, status}) => {
        return await queryOrUpdateDB(
            `UPDATE student_note_tab 
                    set snt_tuition_id = COALESCE(${tuitionId ? `"${tuitionId}"` : null},snt_tuition_id),
                        snt_url = COALESCE(${url ? `"${url}"` : null},snt_url),
                        snt_title = COALESCE(${title ? `"${title}"` : null},snt_title),
                        snt_status = COALESCE(${status ? `"${status}"` : null},snt_status),
                        snt_record_update_date = now() where snt_id = "${noteId}"`);
    },
};


