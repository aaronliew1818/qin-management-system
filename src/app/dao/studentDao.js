import {insertDB, queryOrUpdateDB} from "../../services/db";
import {
    ITEM_TYPE_TUITION,
    ITEM_TYPE_DAYCARE,
    SUBSCRIPTION_ACTIVE,
    SUBSCRIPTION_DELETED,
    STATUS_STUDENT_ACTIVE,
    SUBSCRIPTION_CANCELLED,
    STATUS_STUDENT_TERMINATED,
    STATUS_STUDENT_DEACTIVATED
} from ".././dao/constants";
import _ from "lodash";
import moment from "moment";

export const studentDao = {
    getClassroom: async({schedule, courseId, teacherName, programLvl, enrolmentDate}) => {
        let searchQuery = "";
        if (programLvl != null && programLvl.length > 0 ) {
            searchQuery += ` and st_program_lvl = ${programLvl}`
        }

        if (teacherName != null && teacherName.length > 0){
            teacherName = decodeURIComponent(teacherName);
            searchQuery += ` and tt_name LIKE "%${teacherName}%"`
        }

        if (courseId != null && courseId.length > 0){
            courseId = decodeURIComponent(courseId);
            searchQuery += ` and tft_course_id = "${courseId}"`
        }

        if (schedule != null && schedule.length > 0){
            schedule = decodeURIComponent(schedule);
            searchQuery += ` and st_schedule = "${schedule}"`
        }
        let student = await queryOrUpdateDB(`select
            st_id as studentId, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_phone_no as phoneNo, 
            spl_name as programLvl, tft_course_name as subjectName, st_schedule as schedule, 
            st_enrolment_date as enrolmentDate, tt_name as teacherName
            from student_tab
            inner join subscription_student_course_tab
			on subscription_student_course_tab.ssct_student_id =student_tab.st_id 
			inner join tuition_fee_tab
			on tuition_fee_tab.tft_id = subscription_student_course_tab.ssct_item_id 
			inner join teacher_tab
			on teacher_tab.tt_id = subscription_student_course_tab.ssct_teacher_id 
            inner join student_program_lvl
			on student_program_lvl.spl_id = student_tab.st_program_lvl
			where (ssct_active="${SUBSCRIPTION_ACTIVE}" or ssct_active="${SUBSCRIPTION_CANCELLED}") and st_status="${STATUS_STUDENT_ACTIVE}" 
			and (STR_TO_DATE("${"01-"+enrolmentDate}", "%d-%m-%Y") >= st_enrolment_date or ISNULL(st_enrolment_date)) 
            and CASE WHEN !ISNULL(ssct_expiry_date) and ssct_expiry_date != ""
				THEN (STR_TO_DATE("${enrolmentDate}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y") and STR_TO_DATE(ssct_expiry_date, "%m-%Y")) 
				WHEN ssct_active = "${SUBSCRIPTION_ACTIVE}"
				THEN (STR_TO_DATE("${enrolmentDate}", "%m-%Y") >= STR_TO_DATE(ssct_effective_date, "%m-%Y"))
				ELSE (!ISNULL(ssct_effective_date) AND ssct_expiry_date != "") END
			${searchQuery} 
			order by st_name`);

        if (student!=null && !Array.isArray(student)) {
            student = [{ ...student}];
        } else if (student == null){
            student = [];
        }

        return student;
    },

    getStudents: async({programLvl, name}) => {
        let searchQuery = "";
        if (programLvl != null && programLvl.length > 0 ) {
            searchQuery += ` and st_program_lvl = ${programLvl}`
        }

        if (name != null && name.length > 0){
            name = decodeURIComponent(name);
            searchQuery += ` and st_name LIKE "%${name}%"`
        }

        // let student = await queryOrUpdateDB(`select
        //     st_id as studentId, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_dob as dob,
        //     st_gender as gender, st_phone_no as phoneNo, spl_name as programLvl, st_status as status
        //     from student_tab
        //     inner join student_program_lvl
        //     on student_program_lvl.spl_id = student_tab.st_program_lvl where
        //     (st_status = "${STATUS_STUDENT_ACTIVE}" or st_status = "${STATUS_STUDENT_DEACTIVATED}")
        //     ${searchQuery}
        //     order by spl_id, st_name asc` );
        //
        // const currentDate = moment().format("MM-YYYY")
        // let subscribedCourses = await queryOrUpdateDB(`select student_course_tab.ct_shortform as subjectName, ssct_student_id as studentId
        //     from subscription_student_course_tab
        //     INNER JOIN tuition_fee_tab
        //     ON subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
        //     INNER JOIN student_course_tab
        //     ON tuition_fee_tab.tft_course_id = student_course_tab.ct_id
        //     where ssct_item_type = "${ITEM_TYPE_TUITION}"
        //     and ((STR_TO_DATE("${currentDate}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y") and STR_TO_DATE(ssct_expiry_date, "%m-%Y"))
        //     or ISNULL(ssct_expiry_date) or ssct_expiry_date = "")
        //     and ssct_active != "${SUBSCRIPTION_DELETED}"`);

        // if (!Array.isArray(student)) {
        //     student = [{ ...student}];
        // }

        // if (!Array.isArray(subscribedCourses)) {
        //     subscribedCourses = [{ ...subscribedCourses}];
        // }
        // if (student!=null && subscribedCourses!=null && subscribedCourses.length > 0){
        //     for (let i = 0; i < student.length; i++){
        //         const individualStudentCourses = _.filter(subscribedCourses,  { studentId: student[i].studentId.toString()})
        //         if (individualStudentCourses.length > 0) {
        //             student[i].noOfSubjects = individualStudentCourses[0].noOfSubjects;
        //         } else {
        //             student[i].noOfSubjects = 0;
        //         }
        //     }
        // }

        const currentDate = moment().format("MM-YYYY")
        // let student = await queryOrUpdateDB(`select
        //             st_id as studentId, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_dob as dob,
        //             st_gender as gender, st_phone_no as phoneNo, spl_name as programLvl, GROUP_CONCAT(student_course_tab.ct_shortform SEPARATOR ', ') as subjectName, st_status as status
        //             from student_tab
        //             INNER join student_program_lvl
        //             on student_program_lvl.spl_id = student_tab.st_program_lvl
        //             LEFT JOIN subscription_student_course_tab
        //             ON student_tab.st_id = subscription_student_course_tab.ssct_student_id and subscription_student_course_tab.ssct_active != "${SUBSCRIPTION_CANCELLED}"
        //             LEFT JOIN tuition_fee_tab
        //             ON subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
        //             LEFT JOIN student_course_tab
        //             ON tuition_fee_tab.tft_course_id = student_course_tab.ct_id
        //             where (st_status = "${STATUS_STUDENT_ACTIVE}" or st_status = "${STATUS_STUDENT_DEACTIVATED}" or (st_status = "TERMINATED" and STR_TO_DATE("${currentDate}", "%d-%m-%Y") < st_terminated_date)) and
        //             (ISNULL(ssct_id) or (ssct_item_type = "TUITION"
        //             and (CASE WHEN ssct_active = "${SUBSCRIPTION_ACTIVE}"
        //             THEN (STR_TO_DATE("${currentDate}", "%m-%Y") >= STR_TO_DATE(ssct_effective_date, "%m-%Y"))
        //             WHEN !ISNULL(ssct_expiry_date) and ssct_expiry_date != ""
        //             THEN (STR_TO_DATE("${currentDate}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y") and STR_TO_DATE(ssct_expiry_date, "%m-%Y"))
        //             ELSE (!ISNULL(ssct_effective_date) AND ssct_expiry_date != "") END)
        //             and ssct_active != "${SUBSCRIPTION_DELETED}"))
        //             ${searchQuery}
        //             group by st_id
        //             order by spl_id, st_name asc;`
        // );

        let student = await queryOrUpdateDB(`select * from (
            select st_id as studentId, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_dob as dob,
            st_gender as gender, st_phone_no as phoneNo, spl_id as programLvlId, spl_name as programLvl, GROUP_CONCAT(student_course_tab.ct_shortform SEPARATOR ', ') as subjectName, st_status as status
            from student_tab
            INNER join student_program_lvl
            on student_program_lvl.spl_id = student_tab.st_program_lvl
            LEFT JOIN subscription_student_course_tab
            ON student_tab.st_id = subscription_student_course_tab.ssct_student_id and subscription_student_course_tab.ssct_active != "${SUBSCRIPTION_CANCELLED}"
            LEFT JOIN tuition_fee_tab
            ON subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
            LEFT JOIN student_course_tab
            ON tuition_fee_tab.tft_course_id = student_course_tab.ct_id
            where (st_status = "${STATUS_STUDENT_ACTIVE}" or st_status = "${STATUS_STUDENT_DEACTIVATED}" or (st_status = "TERMINATED" and STR_TO_DATE("${currentDate}", "%d-%m-%Y") < st_terminated_date)) and
            (ISNULL(ssct_id) or (ssct_item_type = "TUITION"
            and (CASE WHEN ssct_active = "${SUBSCRIPTION_ACTIVE}"
                     THEN (STR_TO_DATE("${currentDate}", "%m-%Y") >= STR_TO_DATE(ssct_effective_date, "%m-%Y"))
                     WHEN !ISNULL(ssct_expiry_date) and ssct_expiry_date != ""
                     THEN (STR_TO_DATE("${currentDate}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y") and STR_TO_DATE(ssct_expiry_date, "%m-%Y"))
                     ELSE (!ISNULL(ssct_effective_date) AND ssct_expiry_date != "") END)
            and ssct_active != "${SUBSCRIPTION_DELETED}"))
            ${searchQuery}
            group by studentId
            UNION
            select
            st_id as studentId, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_dob as dob,
            st_gender as gender, st_phone_no as phoneNo, spl_id as programLvlId, spl_name as programLvl, '' as subjectName, st_status as status
            from student_tab
            INNER join student_program_lvl
            on student_program_lvl.spl_id = student_tab.st_program_lvl
            LEFT JOIN subscription_student_course_tab
            ON student_tab.st_id = subscription_student_course_tab.ssct_student_id and subscription_student_course_tab.ssct_active != "${SUBSCRIPTION_CANCELLED}"
            LEFT JOIN tuition_fee_tab
            ON subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
            LEFT JOIN student_course_tab
            ON tuition_fee_tab.tft_course_id = student_course_tab.ct_id
            where (st_status = "${STATUS_STUDENT_ACTIVE}" or st_status = "${STATUS_STUDENT_DEACTIVATED}" or (st_status = "TERMINATED" and STR_TO_DATE("${currentDate}", "%d-%m-%Y") < st_terminated_date))
            ${searchQuery}
            group by studentId) a
            where !ISNULL(studentId)
            group by studentId
            order by programLvlId, name asc`);

        // const currentDate = moment().format("MM-YYYY")
        // let subscribedCourses = await queryOrUpdateDB(`select student_course_tab.ct_shortform as subjectName, ssct_student_id as studentId
        //     from subscription_student_course_tab
        //     INNER JOIN tuition_fee_tab
        //     ON subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
        //     INNER JOIN student_course_tab
        //     ON tuition_fee_tab.tft_course_id = student_course_tab.ct_id
        //     where ssct_item_type = "${ITEM_TYPE_TUITION}"
        //     and ((STR_TO_DATE("${currentDate}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y") and STR_TO_DATE(ssct_expiry_date, "%m-%Y"))
        //     or ISNULL(ssct_expiry_date) or ssct_expiry_date = "")
        //     and ssct_active != "${SUBSCRIPTION_DELETED}"`);

        if (!Array.isArray(student)) {
            student = [{ ...student}];
        }

        return student;
    },

    getNewSubjectEnrollmentStudents: async({programLvl, name, month}) => {
        let searchQuery = "";
        if (programLvl != null && programLvl.length > 0 ) {
            searchQuery += ` and st_program_lvl = ${programLvl}`
        }

        if (name != null && name.length > 0) {
            name = decodeURIComponent(name);
            searchQuery += ` and st_name LIKE "%${name}%"`
        }

        let currentDate;
        if (month == null || month.length === 0) {
            currentDate = moment().format("MM-YYYY")
        } else {
            currentDate = month;
        }
        let student = await queryOrUpdateDB(`select
                    st_id as studentId, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_dob as dob,
                    st_gender as gender, st_phone_no as phoneNo, spl_name as programLvl, GROUP_CONCAT(student_course_tab.ct_shortform SEPARATOR ', ') as subjectName, st_status as status
                    from student_tab
                    INNER join student_program_lvl
                    on student_program_lvl.spl_id = student_tab.st_program_lvl
                    INNER JOIN subscription_student_course_tab
                    ON student_tab.st_id = subscription_student_course_tab.ssct_student_id and subscription_student_course_tab.ssct_active != "${SUBSCRIPTION_CANCELLED}"
                    INNER JOIN tuition_fee_tab
                    ON subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
                    INNER JOIN student_course_tab
                    ON tuition_fee_tab.tft_course_id = student_course_tab.ct_id
                    where (st_status = "${STATUS_STUDENT_ACTIVE}" or st_status = "${STATUS_STUDENT_DEACTIVATED}" or (st_status = "TERMINATED" and STR_TO_DATE("${currentDate}", "%d-%m-%Y") < st_terminated_date)) and
                    (ssct_item_type = "TUITION"
                    and (STR_TO_DATE("${currentDate}", "%m-%Y") = STR_TO_DATE(ssct_effective_date, "%m-%Y"))
                    and ssct_active != "${SUBSCRIPTION_DELETED}")
                    ${searchQuery}
                    group by st_id
                    order by spl_id, st_name asc;`
        );

        if (!Array.isArray(student)) {
            student = [{ ...student}];
        }

        return student;
    },

    getSingleStudentByUuid: async({uuid}) => {
        // tuitionCourse: [ { teacher : "", programLvl: "", subjectId: "", fee: 0 } ],
        //     misc: [ { id : "", name: "", fee: 0 } ],

        let student = await queryOrUpdateDB(`select
            st_id as id, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_dob as dob, 
            st_gender as gender, st_phone_no as phoneNo, st_program_lvl as programLvl, st_email as email,
            st_emergency_contact_name as emergencyContactName, st_emergency_contact_phone_no as emergencyContactPhoneNo,
            st_emergency_contact_relationship as emergencyContactRelationship, st_schedule as schedule, st_enrolment_date as enrolmentDate, st_status as status
            from student_tab where st_uuid = "${uuid}"`);

        const tuitionCourses = [];

        // tuitionCourse: [ { teacher : "", programLvl: "", subjectId: "", fee: 0 } ],
        //subscription can be updated now
        let subscribedCourses = await queryOrUpdateDB(`select *
            from subscription_student_course_tab where ssct_student_id = ${student.id} 
            and ssct_item_type = "${ITEM_TYPE_TUITION}" and ssct_active = "${SUBSCRIPTION_ACTIVE}"`);

        if (!Array.isArray(subscribedCourses) && subscribedCourses!=null) {
            subscribedCourses = [{ ...subscribedCourses}];
        } else if (subscribedCourses == null){
            subscribedCourses = [];
        }

        for (let i = 0; i < subscribedCourses.length; i++) {
            const subscribedCourse = subscribedCourses[i];
            let tuitionFee = await queryOrUpdateDB(`select
                ${subscribedCourse.ssct_id} as subscriptionId, tft_teacher_id as teacher, tft_program_lvl as programLvl, 
                tft_course_id as subjectId, tft_fee as fee
                from tuition_fee_tab where tft_id = ${subscribedCourse.ssct_item_id}`);

            tuitionFee.fee = subscribedCourse.ssct_item_fee;
            tuitionFee.effectiveDate = subscribedCourse.ssct_effective_date;
            tuitionCourses.push(tuitionFee);
        }

        const miscs = [];
        //subscription cannot be deleted. only can be deactivated
        let subscribedMiscs = await queryOrUpdateDB(`select *
            from subscription_student_misc_table where ssmt_student_id = ${student.id} 
            and ssmt_active="${SUBSCRIPTION_ACTIVE}"`);

        if (!Array.isArray(subscribedMiscs) && subscribedMiscs!=null) {
            subscribedMiscs = [{ ...subscribedMiscs}];
        } else if (subscribedMiscs==null) {
            subscribedMiscs = [];
        }


        // misc: [ { id : 1, name: "Student Locker", fee: 10 }, { id : 2, name: "Books", fee: 10 } ],
        for (let i = 0; i < subscribedMiscs.length; i++) {
            const subscribedMisc = subscribedMiscs[i];
            let misc = await queryOrUpdateDB(`select
                ${subscribedMisc.ssmt_id} as subscriptionId, mt_id as id , mt_name as name, mt_fee as fee
                from miscellaneous_tab where mt_id = ${subscribedMisc.ssmt_item_id}`);
            miscs.push(misc);
        }

        return {
            uuid,
            name: student.name,
            nric: student.icNo,
            dob: student.dob,
            phoneNo: student.phoneNo ? student.phoneNo : "",
            email: student.email ? student.email : "",
            gender: student.gender,
            programLvl: student.programLvl,
            emergencyContactName: student.emergencyContactName,
            emergencyContactPhoneNo: student.emergencyContactPhoneNo,
            emergencyContactRelationship: student.emergencyContactRelationship,
            schedule: student.schedule,
            enrolmentDate: student.enrolmentDate ? moment(student.enrolmentDate).utcOffset('+0800').format("YYYY-MM-DD HH:mm:ss") : null,
            tuitionCourses,
            miscs,
            status: student.status
        };
    },

    getStudentInfoByUuuid: async(uuid) => {
        return await queryOrUpdateDB(`select
            * from student_tab
            where st_uuid = "${uuid}"
            limit 1
          `);
    },

    getStudentInfoByIdNo: async(idNo) => {
        return await queryOrUpdateDB(`select
            * from student_tab
            where st_ic_no = "${idNo}"
            limit 1
          `);
    },

    getStudentInfoByNameAndPhoneNo: async(name, phoneNo) => {
        return await queryOrUpdateDB(`select
            * from student_tab
            where st_name = "${name}" and st_phone_no = "${phoneNo}"
            limit 1
          `);
    },

    insertStudentInfo:  async(data) => {
        return await insertDB(`INSERT INTO student_tab 
          ( st_name, st_ic_no, st_gender, st_phone_no, st_email, st_program_lvl, 
          st_emergency_contact_name, st_emergency_contact_phone_no, st_emergency_contact_relationship, 
          st_schedule, st_enrolment_date, st_record_create_data, st_record_update_date, st_uuid)
          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now(), uuid())`, data);
    },

    updateStudentInfo:  async({name, nric, gender, phoneNo, programLvl, uuid, email,
                                  emergencyContactName, emergencyContactPhoneNo, emergencyContactRelationship, schedule, enrolmentDate}) => {

        if (enrolmentDate) {
            enrolmentDate = moment(enrolmentDate, "MM-YYYY").format("YYYY-MM-DD HH:mm:ss");
        }
        return await queryOrUpdateDB(`UPDATE student_tab 
            SET st_name = "${name}", 
            st_ic_no = "${nric}",
            st_gender = "${gender}", 
            st_phone_no = "${phoneNo}", 
            st_program_lvl = ${programLvl}, 
            st_email = "${email}", 
            st_emergency_contact_name = "${emergencyContactName}", 
            st_emergency_contact_phone_no = "${emergencyContactPhoneNo}", 
            st_emergency_contact_relationship = "${emergencyContactRelationship}", 
            st_schedule = "${schedule}", 
            st_enrolment_date = COALESCE(${enrolmentDate? `"${enrolmentDate}"` : null},st_enrolment_date), 
            st_record_update_date = now() where st_uuid = "${uuid}"`);
    },

    updateStudentStatus:  async({uuid, status, terminatedDate}) => {
        if (terminatedDate) {
            terminatedDate = moment(terminatedDate, "MM-YYYY").format("YYYY-MM-DD HH:mm:ss");
        }

        if (status === STATUS_STUDENT_TERMINATED) {
            return await queryOrUpdateDB(`UPDATE student_tab 
            SET st_status = "${status}", 
            st_terminated_date = "${terminatedDate}",
            st_record_update_date = now() where st_uuid = "${uuid}"`);
        } else {
            return await queryOrUpdateDB(`UPDATE student_tab 
            SET st_status = "${status}", 
            st_record_update_date = now() where st_uuid = "${uuid}"`);
        }
    },


    getStudentSubscriptionInfoById: async(id) => {
        return await queryOrUpdateDB(`select
            * from subscription_student_course_tab
            where ssct_id = "${id}"
            limit 1
          `);
    },
};
