import {insertDB, queryOrUpdateDB} from "../../services/db";
import _ from "lodash";
import moment from 'moment';
import {
    DATE_FORMAT_RESPONSE, SUBSCRIPTION_ACTIVE, PAYMENT_STATUS_TEACHER_NEW,
    PAYMENT_STATUS_TEACHER_PAID, PAYMENT_STATUS_TEACHER_NOT_TALLY,
    PAYMENT_STATUS_STUDENT_PENDING, PAYMENT_STATUS_STUDENT_PAID, PAYMENT_STATUS_STUDENT_PARTIALLY_PAID,
    PAYMENT_ITEM_TYPE_MISC, PAYMENT_ITEM_TYPE_COURSE, SUBSCRIPTION_DELETED, PAYMENT_TYPE_TEACHER_RATIO,
    PAYMENT_TYPE_TEACHER_HOUR, PAYMENT_STATUS_PAID, STATUS_STUDENT_ACTIVE, SUBSCRIPTION_CANCELLED
} from ".././dao/constants";
import {teacherDao} from "./teacherDao";

function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

export const paymentDao = {
    getStudentsPayment : async({month, programLvl, name}) => {
        let searchQuery = "";
        if (programLvl != null && programLvl.length > 0){
            searchQuery += ` and st_program_lvl = ${programLvl}`
        }

        if (name != null && name.length > 0){
            searchQuery += ` and st_name like '%${name}%'`
        }

        let studentsTotalPayment = await queryOrUpdateDB(`SELECT st_uuid as uuid, 
                st_name as name, spl_name as programLvl, st_ic_no as icNo, ssct_id as subscriptionId, 
                sum(ssct_item_fee) as totalTuitionFee,
                "${month}" as "month",
                count(*) as noOfSubjects
				FROM qms.subscription_student_course_tab
				inner join student_tab
				on student_tab.st_id = subscription_student_course_tab.ssct_student_id
				inner join student_program_lvl
				on student_program_lvl.spl_id = student_tab.st_program_lvl
				inner join tuition_fee_tab
				on tuition_fee_tab.tft_id = subscription_student_course_tab.ssct_item_id
				where 
				CASE WHEN !ISNULL(ssct_expiry_date) and ssct_expiry_date != ""
				THEN (STR_TO_DATE("${month}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y") and STR_TO_DATE(ssct_expiry_date, "%m-%Y")) 
				WHEN ssct_active = "${SUBSCRIPTION_ACTIVE}"
				THEN (STR_TO_DATE("${month}", "%m-%Y") >= STR_TO_DATE(ssct_effective_date, "%m-%Y"))
				ELSE (!ISNULL(ssct_effective_date) AND ssct_expiry_date != "") END
				and ssct_active != "${SUBSCRIPTION_DELETED}"
				and st_status = "${STATUS_STUDENT_ACTIVE}"
				${searchQuery}
				group by ssct_student_id
				order by spl_id, st_name asc`);

        let studentsPaidAmount = await queryOrUpdateDB(`SELECT st_uuid, 
                st_name, st_program_lvl, st_ic_no, ssct_id,  sum(pst_total_amount) as totalTuitionFee,
                pst_transaction_date, pst_transaction_type, count(*) as noOfSubjects
				FROM qms.subscription_student_course_tab
				inner join payment_student_table
				on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
				inner join student_tab
				on student_tab.st_id = subscription_student_course_tab.ssct_student_id
				where pst_status = "${PAYMENT_STATUS_STUDENT_PAID}" 
				and pst_item_type = "${PAYMENT_ITEM_TYPE_COURSE}"
				and pst_month="${month}" 
				${searchQuery}
				group by ssct_student_id`);


        let studentsTotalPaymentArray = [];
        if (studentsTotalPayment!=null && !isArray(studentsTotalPayment)){
            studentsTotalPaymentArray.push(studentsTotalPayment);
        } else if (studentsTotalPayment!=null) {
            studentsTotalPaymentArray = [...studentsTotalPayment];
        }

        let studentsPaidAmountArray = [];
        if (studentsPaidAmount !=null && !isArray(studentsPaidAmount)){
            studentsPaidAmountArray.push(studentsPaidAmount);
        } else if (studentsPaidAmount!=null) {
            studentsPaidAmountArray = [...studentsPaidAmount];
        }

        for (let i = 0; i < studentsTotalPaymentArray.length; i ++){
            //filter the studentsPaidAmount by student id
            const individualStudentPayment = _.filter(studentsPaidAmountArray,  { st_uuid:  studentsTotalPaymentArray[i].uuid})
            const subscribedAmount = studentsTotalPaymentArray[i].totalTuitionFee;
            if (individualStudentPayment!=null && individualStudentPayment.length > 0) {
                let paidAmount = 0;
                //calculate total paid amount
                for (let i = 0; i < individualStudentPayment.length; i ++) {
                    paidAmount+= individualStudentPayment[i].totalTuitionFee;
                }
                // const totalOwedFee = subscribedAmount - paidAmount;
                // studentsTotalPaymentArray[i].totalOwedFee = totalOwedFee;
                // if (totalOwedFee === 0){
                //     studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
                //     //assume that there is only one unique uuid
                //     studentsTotalPaymentArray[i].transactionDate = moment(individualStudentPayment[0].pst_transaction_date, "YYYY-MM-DD")
                //                                                     .format(DATE_FORMAT_RESPONSE);
                // } else {
                //     studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
                // }

                studentsTotalPaymentArray[i].totalOwedFee = paidAmount;
                if (studentsTotalPaymentArray[i].noOfSubjects >  individualStudentPayment[0].noOfSubjects){
                    //it is partially payment if the no of subscribed subjects are more than the paid subjects
                    //temporary change this to paid assume that there is no partial paid
                    studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
                    // studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PARTIALLY_PAID;
                } else {
                    studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
                }
                //assume that there is only one unique uuid
                studentsTotalPaymentArray[i].transactionDate = moment(individualStudentPayment[0].pst_transaction_date, "DD-MM-YYYY")
                    .format(DATE_FORMAT_RESPONSE);
                studentsTotalPaymentArray[i].transactionType = individualStudentPayment[0].pst_transaction_type;

            } else {
                studentsTotalPaymentArray[i].totalOwedFee = subscribedAmount;
                studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
            }
        }

        return studentsTotalPaymentArray;
    },

    getStudentPaymentDetail : async({uuid, month}) => {
        let studentsTotalPayment = await queryOrUpdateDB(`SELECT st_uuid as uuid, 
                st_name as name, spl_name as programLvl, 
                st_ic_no as icNo, ssct_id as subscriptionId, 
                ct_subject as subjectName,
                ssct_item_fee as fee,
                "${month}" as "month"
				FROM qms.subscription_student_course_tab
				inner join student_tab
				on student_tab.st_id = subscription_student_course_tab.ssct_student_id
				inner join student_program_lvl
				on student_program_lvl.spl_id = student_tab.st_program_lvl
				inner join tuition_fee_tab
				on tuition_fee_tab.tft_id = subscription_student_course_tab.ssct_item_id
				inner join student_course_tab
				on student_course_tab.ct_id = tuition_fee_tab.tft_course_id
				where CASE WHEN !ISNULL(ssct_expiry_date) and ssct_expiry_date != ""
				THEN (STR_TO_DATE("${month}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y") and STR_TO_DATE(ssct_expiry_date, "%m-%Y")) 
				WHEN ssct_active = "${SUBSCRIPTION_ACTIVE}"
				THEN (STR_TO_DATE("${month}", "%m-%Y") >= STR_TO_DATE(ssct_effective_date, "%m-%Y"))
				ELSE (!ISNULL(ssct_effective_date) AND ssct_expiry_date != "") END
				and ssct_active != "${SUBSCRIPTION_DELETED}"
				and st_uuid="${uuid}"`);

        //get the fee from payment table, not subscription fee
        let studentsPaidAmount = await queryOrUpdateDB(`SELECT st_uuid, 
                st_name, st_program_lvl, st_ic_no, ssct_id,  pst_total_amount as fee, pst_item_subcription_id,
                pst_transaction_date as transactionDate,
                pst_transaction_type as transactionType
				FROM qms.subscription_student_course_tab
				inner join payment_student_table
				on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
				inner join student_tab
				on student_tab.st_id = subscription_student_course_tab.ssct_student_id
				where pst_status = "${PAYMENT_STATUS_STUDENT_PAID}" 
				and pst_item_type = "${PAYMENT_ITEM_TYPE_COURSE}"
				and pst_month="${month}" 
				and st_uuid="${uuid}"`);

        let studentsTotalPaymentArray = [];
        if (studentsTotalPayment!=null && !isArray(studentsTotalPayment)){
            studentsTotalPaymentArray.push(studentsTotalPayment);
        } else if (studentsTotalPayment!=null) {
            studentsTotalPaymentArray = [...studentsTotalPayment];
        }

        let studentsPaidAmountArray = [];
        if (studentsPaidAmount !=null && !isArray(studentsPaidAmount)){
            studentsPaidAmountArray.push(studentsPaidAmount);
        } else if (studentsPaidAmount!=null) {
            studentsPaidAmountArray = [...studentsPaidAmount];
        }

        for (let i = 0; i < studentsTotalPaymentArray.length; i ++){
            //filter the studentsPaidAmount by student id
            const individualStudentPayment = _.filter(studentsPaidAmountArray,  { pst_item_subcription_id:  studentsTotalPaymentArray[i].subscriptionId})
            const subscribedAmount = studentsTotalPaymentArray[i].fee;
            if (individualStudentPayment!=null && individualStudentPayment.length > 0) {
                let paidAmount = 0;
                //calculate total paid amount
                for (let i = 0; i < individualStudentPayment.length; i ++) {
                    paidAmount+= individualStudentPayment[i].fee;
                }

                studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
                if (individualStudentPayment.length> 0){
                    studentsTotalPaymentArray[i].transactionDate = individualStudentPayment[0].transactionDate;
                    studentsTotalPaymentArray[i].transactionType = individualStudentPayment[0].transactionType;
                }
                studentsTotalPaymentArray[i].fee = paidAmount;
                // const totalOwedFee = subscribedAmount - paidAmount;
                // // studentsTotalPaymentArray[i].totalOwedFee = totalOwedFee;
                // if (totalOwedFee === 0){
                //     studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
                //     if (individualStudentPayment.length> 0){
                //         studentsTotalPaymentArray[i].transactionDate = individualStudentPayment[0].transactionDate;
                //         studentsTotalPaymentArray[i].transactionType = individualStudentPayment[0].transactionType;
                //     }
                // } else {
                //     studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
                // }

            } else {
                // studentsTotalPaymentArray[i].totalOwedFee = subscribedAmount;
                studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
            }
        }
        return studentsTotalPaymentArray;
    },
    // getStudentsMiscPaymentByMonth: async({month, programLvl, name, isPaid}) => {
    //     let searchQuery = "";
    //     if (programLvl != null && programLvl.length > 0){
    //         searchQuery += ` and st_program_lvl="${programLvl}"`
    //     }
    //
    //     if (name != null && name.length > 0){
    //         searchQuery += ` and st_name like '%${name}%'`
    //     }
    //
    //     let studentsTotalPayment = await queryOrUpdateDB(`SELECT st_uuid as uuid,
    //             st_name as name, spl_name as programLvl, st_ic_no as icNo, ssmt_id as subscriptionId,
    //             sum(mt_fee) as totalTuitionFee,
    //             "${month}" as "month"
	// 			FROM qms.subscription_student_misc_table
	// 			inner join student_tab
	// 			on student_tab.st_id = subscription_student_misc_table.ssmt_student_id
	// 			inner join student_program_lvl
	// 			on student_program_lvl.spl_id = student_tab.st_program_lvl
	// 			inner join miscellaneous_tab
	// 			on miscellaneous_tab.mt_id = subscription_student_misc_table.ssmt_item_id
	// 			where (STR_TO_DATE("${month}", "%m-%Y") BETWEEN STR_TO_DATE(ssmt_effective_date, "%m-%Y") and STR_TO_DATE(ssmt_expiry_date, "%m-%Y") or ISNULL(ssmt_expiry_date) or ssmt_expiry_date = "")
	// 			and (STR_TO_DATE("${month}", "%m-%Y") >= STR_TO_DATE(ssmt_effective_date, "%m-%Y"))
	// 			and ssmt_active != "${SUBSCRIPTION_DELETED}"
	// 			${searchQuery}
	// 			group by ssmt_student_id`);
    //
    //     let studentsPaidAmount = await queryOrUpdateDB(`SELECT st_uuid,
    //             st_name, st_program_lvl, st_ic_no, ssmt_id,  sum(pst_total_amount) as totalTuitionFee
	// 			FROM qms.subscription_student_misc_table
	// 			inner join payment_student_table
	// 			on payment_student_table.pst_item_subcription_id = subscription_student_misc_table.ssmt_id
	// 			inner join student_tab
	// 			on student_tab.st_id = subscription_student_misc_table.ssmt_student_id
	// 			where pst_status = "${PAYMENT_STATUS_STUDENT_PAID}" and pst_month="${month}"
	// 			and pst_item_type = "${PAYMENT_ITEM_TYPE_MISC}"
	// 			${searchQuery}
	// 			group by ssmt_student_id`);
    //     let studentsTotalPaymentArray = [];
    //     if (studentsTotalPayment!=null && !isArray(studentsTotalPayment)){
    //         studentsTotalPaymentArray.push(studentsTotalPayment);
    //     } else if (studentsTotalPayment!=null) {
    //         studentsTotalPaymentArray = [...studentsTotalPayment];
    //     }
    //
    //     let studentsPaidAmountArray = [];
    //     if (studentsPaidAmount !=null && !isArray(studentsPaidAmount)){
    //         studentsPaidAmountArray.push(studentsPaidAmount);
    //     } else if (studentsPaidAmount!=null) {
    //         studentsPaidAmountArray = [...studentsPaidAmount];
    //     }
    //
    //     for (let i = 0; i < studentsTotalPaymentArray.length; i ++){
    //         //filter the studentsPaidAmount by student id
    //         const individualStudentPayment = _.filter(studentsPaidAmountArray,  { st_uuid:  studentsTotalPaymentArray[i].uuid})
    //         const subscribedAmount = studentsTotalPaymentArray[i].totalTuitionFee;
    //         if (individualStudentPayment!=null && individualStudentPayment.length > 0) {
    //             let paidAmount = 0;
    //             //calculate total paid amount
    //             for (let i = 0; i < individualStudentPayment.length; i ++) {
    //                 paidAmount+= individualStudentPayment[i].totalTuitionFee;
    //             }
    //             const totalOwedFee = subscribedAmount - paidAmount;
    //             studentsTotalPaymentArray[i].totalOwedFee = totalOwedFee;
    //             if (totalOwedFee === 0){
    //                 studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
    //             } else {
    //                 studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
    //             }
    //
    //         } else {
    //             studentsTotalPaymentArray[i].totalOwedFee = subscribedAmount;
    //             studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
    //         }
    //     }
    //     return studentsTotalPaymentArray;
    // },
    getStudentsMiscPaymentByMonth: async({month, programLvl, name, itemId, isPaid}) => {
        let searchQuery = "";
        if (programLvl != null && programLvl.length > 0){
            searchQuery += ` and st_program_lvl="${programLvl}"`
        }

        if (name != null && name.length > 0){
            searchQuery += ` and st_name like '%${name}%'`
        }


        //TODO: Do a special operation for the book fee

        // let firstHalf = ` MONTH(STR_TO_DATE(pst_month, "%m-%Y")) >= 1 and MONTH(STR_TO_DATE(pst_month, "%m-%Y")) <= 6
        // AND YEAR(STR_TO_DATE(pst_month, "%m-%Y")) = YEAR(STR_TO_DATE(pst_month, "%m-%Y"))`;

        // let secondHalf = ` MONTH(STR_TO_DATE(pst_month, "%m-%Y")) >= 7 and MONTH(STR_TO_DATE(pst_month, "%m-%Y")) <= 12
        // AND YEAR(STR_TO_DATE(${month}, "%m-%Y")) = YEAR(STR_TO_DATE(pst_month, "%m-%Y"))`;

        // let monthQuery = "  ";
        // if (itemId.toString() === "2"){
        //     monthQuery = firstHalf;
        // } else {
        //     monthQuery = secondHalf;
        // }

        let monthQuery = ` YEAR(STR_TO_DATE("${month}", "%m-%Y")) = YEAR(STR_TO_DATE(pst_month, "%m-%Y"))`;

        let studentsPayment = await queryOrUpdateDB(`SELECT *, sum(pst_total_amount)  as totalAmount
                FROM qms.student_tab 
                left join qms.payment_student_table
                on student_tab.st_id = payment_student_table.pst_student_id
                where pst_status = "${PAYMENT_STATUS_PAID}" and ${monthQuery}
                and pst_item_type = "${PAYMENT_ITEM_TYPE_MISC}" and pst_item_id=${itemId}
                group by pst_student_id;`);

        let students = await queryOrUpdateDB(`select
            st_id as studentId, st_uuid as uuid, st_name as name, st_ic_no as icNo, st_dob as dob, 
            st_gender as gender, st_phone_no as phoneNo, spl_name as programLvl 
            from student_tab 
            inner join student_program_lvl 
            on student_program_lvl.spl_id = student_tab.st_program_lvl 
            where st_status = "${STATUS_STUDENT_ACTIVE}"
            ${searchQuery} 
            order by spl_id, st_name asc` );

        let studentsPaymentArray = [];
        if (studentsPayment!=null && !isArray(studentsPayment)){
            studentsPaymentArray.push(studentsPayment);
        } else if (studentsPayment!=null) {
            studentsPaymentArray = [...studentsPayment];
        }

        let studentsArray = [];
        if (students!=null && !isArray(students)){
            studentsArray.push(students);
        } else if (students!=null) {
            studentsArray = [...students];
        }

        for (let i = 0; i < studentsArray.length; i ++){
            //filter the studentsPaidAmount by student id
            const individualStudentPayment = _.filter(studentsPaymentArray,  { st_id:  studentsArray[i].studentId})
            let totalAmount = 0;
            if (individualStudentPayment.length > 0){
                totalAmount = individualStudentPayment[0].totalAmount;
            }

            if (individualStudentPayment.length > 0) {
                studentsArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
                studentsArray[i].totalOwedFee = totalAmount;
            } else {
                studentsArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
            }
        }
        return studentsArray;
    },
    getStudentMiscPaymentDetail : async({uuid, month}) => {
        let studentsTotalPayment = await queryOrUpdateDB(`SELECT st_uuid as uuid, 
                st_name as name, spl_name as programLvl, 
                st_ic_no as icNo, ssmt_id as subscriptionId, 
                mt_name as itemName,
                mt_fee as fee,
                "${month}" as "month"
				FROM qms.subscription_student_misc_table
				inner join student_tab
				on student_tab.st_id = subscription_student_misc_table.ssmt_student_id
				inner join student_program_lvl
				on student_program_lvl.spl_id = student_tab.st_program_lvl
				inner join miscellaneous_tab
				on miscellaneous_tab.mt_id = subscription_student_misc_table.ssmt_item_id
				where (STR_TO_DATE("${month}", "%m-%Y") BETWEEN STR_TO_DATE(ssmt_effective_date, "%m-%Y") and STR_TO_DATE(ssmt_expiry_date, "%m-%Y") or ISNULL(ssmt_expiry_date) or ssmt_expiry_date = "")
				and (STR_TO_DATE("${month}", "%m-%Y") >= STR_TO_DATE(ssmt_effective_date, "%m-%Y"))
				and ssmt_active != "${SUBSCRIPTION_DELETED}"
				and st_uuid="${uuid}"`);

        //get the fee from payment table, not subscription fee
        let studentsPaidAmount = await queryOrUpdateDB(`SELECT st_uuid, 
                st_name, st_program_lvl, st_ic_no, ssmt_id,  pst_total_amount as fee, pst_item_subcription_id
				FROM qms.subscription_student_misc_table
				inner join payment_student_table
				on payment_student_table.pst_student_id = subscription_student_misc_table.ssmt_student_id
				inner join student_tab
				on student_tab.st_id = subscription_student_misc_table.ssmt_student_id
				where pst_status = "${PAYMENT_STATUS_STUDENT_PAID}" 
				and pst_item_type = "${PAYMENT_ITEM_TYPE_MISC}"
				and pst_month="${month}" and st_uuid="${uuid}"`);

        let studentsTotalPaymentArray = [];
        if (studentsTotalPayment!=null && !isArray(studentsTotalPayment)){
            studentsTotalPaymentArray.push(studentsTotalPayment);
        } else if (studentsTotalPayment!=null) {
            studentsTotalPaymentArray = [...studentsTotalPayment];
        }

        let studentsPaidAmountArray = [];
        if (studentsPaidAmount !=null && !isArray(studentsPaidAmount)){
            studentsPaidAmountArray.push(studentsPaidAmount);
        } else if (studentsPaidAmount!=null) {
            studentsPaidAmountArray = [...studentsPaidAmount];
        }

        for (let i = 0; i < studentsTotalPaymentArray.length; i ++){
            //filter the studentsPaidAmount by student id
            const individualStudentPayment = _.filter(studentsPaidAmountArray,  { pst_item_subcription_id:  studentsTotalPaymentArray[i].subscriptionId})
            const subscribedAmount = studentsTotalPaymentArray[i].fee;
            if (individualStudentPayment!=null && individualStudentPayment.length > 0) {
                let paidAmount = 0;
                //calculate total paid amount
                for (let i = 0; i < individualStudentPayment.length; i ++) {
                    paidAmount+= individualStudentPayment[i].fee;
                }
                const totalOwedFee = subscribedAmount - paidAmount;
                // studentsTotalPaymentArray[i].totalOwedFee = totalOwedFee;
                if (totalOwedFee === 0){
                    studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PAID;
                } else {
                    studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
                }

            } else {
                // studentsTotalPaymentArray[i].totalOwedFee = subscribedAmount;
                studentsTotalPaymentArray[i].paymentStatus = PAYMENT_STATUS_STUDENT_PENDING;
            }
        }
        return studentsTotalPaymentArray;
    },
    // getAllStudentsMiscPaymentByMonth: async({month, programLvl, name, isPaid}) => {
    //     let searchQuery = "";
    //     if (programLvl != null && programLvl.length > 0){
    //         console.log('enter program')
    //         searchQuery += searchQuery === "" ? "" : " and "
    //         searchQuery += `programLvl = ${programLvl}`
    //     }
    //
    //     if (name != null && name.length > 0){
    //         console.log('enter name')
    //         searchQuery += searchQuery === "" ? "" : " and "
    //         searchQuery += `name like '%${name}%'`
    //     }
    //
    //     if (isPaid != null && isPaid.length > 0){
    //         searchQuery += searchQuery === "" ? "" : " and "
    //         searchQuery += `isPaid = ${isPaid}`
    //     }
    //     const totalAmountForEachStudents = await insertDB(`
    //         CALL getStudentMiscPayments(?, ?);
    //     `, [month, searchQuery]);
    //
    //     return [...totalAmountForEachStudents[0]];
    // },

    getSingleStudentMiscPaymentDetail : async({studentUuid, month}) => {
        const singleStudentMiscPaymentDetail = await insertDB(`
            CALL getSingleStudentMiscPayment(?, ?);
        `, [month, studentUuid]);
        return [...singleStudentMiscPaymentDetail[0]];
    },

    getSingleStudentPaymentDetail : async(studentId, month, subscriptionId) => {
        let studentPaymentDetail = await queryOrUpdateDB(`select *
            from payment_student_table 
            where pst_month = "${month}" 
            and pst_student_id = ${studentId}
            and pst_item_subcription_id = ${subscriptionId};`);
        return studentPaymentDetail;
    },

    insertPaymentData:  async(data) => {
        return await insertDB(`INSERT INTO payment_student_table 
          ( pst_payment_type, pst_month, pst_total_amount, pst_status, pst_student_id, pst_student_name, pst_teacher_id, 
          pst_teacher_name, pst_item_type, pst_item_id, pst_item_name, pst_item_subcription_id, pst_transaction_date,
          pst_transaction_type, pst_created_date, pst_updated_date )
          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())`, data);
    },

    updatePaymentDataByPaymentId:  async(paymentId, transactionDate, transactionType) => {
        return await insertDB(`UPDATE payment_student_table set
          pst_transaction_date = "${transactionDate}", pst_transaction_type = "${transactionType}", pst_updated_date = now() 
          where pst_id=${paymentId}`);
    },


    insertTeacherPayment:  async(data) => {
        return await insertDB(`INSERT INTO payment_teacher_table 
          ( ptt_payment_type, ptt_month, ptt_total_amount, ptt_status, ptt_teacher_id, 
          ptt_item_type, ptt_created_date, ptt_updated_date )
          VALUES ( ?, ?, ?, ?, ?, ?, now(), now())`, data);
    },

    // insertStudentMiscPaymentData:  async(data) => {
    //     return await insertDB(`INSERT INTO payment_student_table
    //       ( pst_payment_type, pst_month, pst_total_amount, pst_status, pst_student_id, pst_student_name,
    //       pst_item_type, pst_item_id, pst_item_name, pst_item_subcription_id, pst_created_date, pst_updated_date )
    //       VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())`, data);
    // },

    insertStudentMiscPaymentData:  async(data) => {
        return await insertDB(`INSERT INTO payment_student_table 
          ( pst_payment_type, pst_month, pst_total_amount, pst_status, pst_student_id, pst_student_name, 
          pst_item_type, pst_item_id, pst_item_name, pst_transaction_date, pst_transaction_type, pst_created_date, pst_updated_date )
          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())`, data);
    },

    subscribeMisc: async(data) => {
        return await insertDB(`INSERT INTO subscription_student_misc_table 
          (ssmt_student_id, ssmt_student_name, ssmt_item_id, ssmt_item_name, ssmt_fee, ssmt_active, ssmt_recurrence_type, ssmt_recurrence_duration, ssmt_created_date, ssmt_updated_date)
          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, now(), now())`, data);
    },

    getAllCourses: async() => {
        return await queryOrUpdateDB(`select
            ct_id as id, ct_subject as name from student_course_tab`);
    },

    getProgramLvl: async() => {
        return await queryOrUpdateDB(`select
            spl_id as id, spl_name as name from student_program_lvl`);
    },

    checkIfTeacherIsPaid : async(month, teacherId) => {
        let teacherSalary = await queryOrUpdateDB(`select *
            from payment_teacher_table 
            where ptt_month = "${month}" 
            and ptt_teacher_id = ${teacherId};`);
        return teacherSalary;
    },

    getTeacherTotalSalaryById : async(month, teacherId) => {
        let teacherSalary = await queryOrUpdateDB(`        
            select IF(tt_payment_type="HOUR", "NA", CONVERT(sum(pst_total_amount) * tt_payment_rate, INT)) as totalSalary from (
            select *
            from payment_student_table
            INNER JOIN teacher_tab
            on payment_student_table.pst_teacher_id = teacher_tab.tt_id
            INNER JOIN subscription_student_course_tab
            on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
            where (DATE_FORMAT(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y"), "%m-%Y")="${month}")
            and STR_TO_DATE(pst_month, "%m-%Y") <= STR_TO_DATE("${month}", "%m-%Y")
            and pst_teacher_id=${teacherId}
            UNION
            select *
            from payment_student_table
            INNER JOIN teacher_tab
            on payment_student_table.pst_teacher_id = teacher_tab.tt_id
            INNER JOIN subscription_student_course_tab
            on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
            where STR_TO_DATE(pst_month, "%m-%Y") > STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")
            and pst_month = "${month} and pst_teacher_id=${teacherId}") t`);
        // let teacherSalary = await queryOrUpdateDB(`select CONVERT(sum(pst_total_amount) * tt_payment_rate, INT) as totalSalary
        //     from payment_student_table
        //     where DATE_FORMAT(pst_created_date, "%m-%Y")="${month}"
        //     and STR_TO_DATE(pst_month, "%m-%Y") <= STR_TO_DATE("${month}", "%m-%Y")
        //     and pst_teacher_id=${teacherId};`);
        return teacherSalary;
    },

    // getTeacherTotalSalaryById : async(month, teacherId) => {
    //     let teacherSalary = await queryOrUpdateDB(`select sum(ssct_item_fee) as totalSalary
    //         from subscription_student_course_tab
    //         where ((STR_TO_DATE("${month}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y")
    //         and STR_TO_DATE(ssct_expiry_date, "%m-%Y")) or ISNULL(ssct_expiry_date) or ssct_expiry_date = "")
    //         and ssct_active="${SUBSCRIPTION_ACTIVE}"
    //         and ssct_teacher_id=${teacherId};`);
    //     return teacherSalary;
    // },


    //get salary based on payment
    getTeachersSalary : async({month}) => {
        //first query is handle the late and on time payment,
        // 2nd query is the advance payment
        let teacherSalary = await queryOrUpdateDB(`select pst_id as paymentId, pst_teacher_id as id, 
            tt_uuid as uuid,
            tt_name as name, tt_ic as icNo, tt_phone_no as phoneNo,
            tt_payment_type as paymentType,
            tt_payment_rate as paymentRate,
            "${month}" as "month",
            IF(tt_payment_type="HOUR", "NA", CONVERT(sum(pst_total_amount) * tt_payment_rate, INT)) as totalSalary from (
            select *
            from payment_student_table 
            INNER JOIN teacher_tab
            on payment_student_table.pst_teacher_id = teacher_tab.tt_id
            INNER JOIN subscription_student_course_tab
            on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
            where (DATE_FORMAT(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y"), "%m-%Y")="${month}")
            and STR_TO_DATE(pst_month, "%m-%Y") <= STR_TO_DATE("${month}", "%m-%Y")
            UNION
            select *
            from payment_student_table 
            INNER JOIN teacher_tab
            on payment_student_table.pst_teacher_id = teacher_tab.tt_id
            INNER JOIN subscription_student_course_tab
            on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
            where STR_TO_DATE(pst_month, "%m-%Y") > STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")
            and pst_month = "${month}") t group by pst_teacher_id`);

        // (DATE_FORMAT(pst_created_date, "%m-%Y")="${month}")
        // and STR_TO_DATE(pst_month, "%m-%Y") <= STR_TO_DATE("${month}", "%m-%Y")
        let teacherSalariesArray = [];
        if (teacherSalary!=null && !isArray(teacherSalary)){
            teacherSalariesArray.push(teacherSalary);
        } else if (teacherSalary!=null){
            teacherSalariesArray =  [...teacherSalary];
        }

        for (let i = 0; i < teacherSalariesArray.length; i ++){
            let teacherPayment = await queryOrUpdateDB(`select * FROM 
                payment_teacher_table
                where ptt_teacher_id=${teacherSalariesArray[i].id} 
                and ptt_month="${month}"`);
            if (teacherPayment == null){
                teacherSalariesArray[i].status = PAYMENT_STATUS_TEACHER_NEW;
            } else if (teacherSalariesArray[i].paymentType === PAYMENT_TYPE_TEACHER_RATIO && teacherSalariesArray[i].totalSalary !== Math.round(teacherPayment.ptt_total_amount)){
                teacherSalariesArray[i].totalPayment = Math.round(teacherPayment.ptt_total_amount);
                teacherSalariesArray[i].status = PAYMENT_STATUS_TEACHER_NOT_TALLY;
            } else if (teacherSalariesArray[i].paymentType === PAYMENT_TYPE_TEACHER_HOUR){
                teacherSalariesArray[i].status = PAYMENT_STATUS_TEACHER_PAID;
                teacherSalariesArray[i].transactionDate = moment(teacherPayment.ptt_updated_date)
                    .format(DATE_FORMAT_RESPONSE);
                teacherSalariesArray[i].totalSalary = Math.round(teacherPayment.ptt_total_amount)

            } else {
                teacherSalariesArray[i].status = PAYMENT_STATUS_TEACHER_PAID;
                teacherSalariesArray[i].transactionDate = moment(teacherPayment.ptt_updated_date)
                    .format(DATE_FORMAT_RESPONSE);
            }
        }

        return teacherSalariesArray;
    },

    getTeacherSalaryDetail : async({month, teacherUuid}, teacherByUuid) => {
        let teacherSalary;
        if (teacherByUuid.tt_payment_type === PAYMENT_TYPE_TEACHER_HOUR){
            teacherSalary = await queryOrUpdateDB(`
                select ptt_id as paymentId, ptt_total_amount as fee 
                from payment_teacher_table 
                where ptt_teacher_id=${teacherByUuid.tt_id};` );
        } else {
            teacherSalary = await queryOrUpdateDB(`
            select pst_id as paymentId, pst_item_subcription_id as subscriptionId,
            st_name as studentName, spl_name as programLvl, pst_total_amount as fee,
            pst_total_amount as fee,
            pst_month as paymentDate,
            pst_transaction_date as transactionDate,
            ct_id as subjectId, ct_subject as subjectName from (
            select * from payment_student_table 
            INNER JOIN student_tab
            on payment_student_table.pst_student_id = student_tab.st_id
            INNER JOIN teacher_tab
            on payment_student_table.pst_teacher_id = teacher_tab.tt_id
            INNER JOIN student_course_tab
            on payment_student_table.pst_item_id = student_course_tab.ct_id
            INNER JOIN subscription_student_course_tab
            on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
            INNER JOIN tuition_fee_tab
            on subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
            INNER JOIN student_program_lvl
            on tuition_fee_tab.tft_program_lvl = student_program_lvl.spl_id
            where pst_teacher_id=${teacherByUuid.tt_id}
            and (DATE_FORMAT(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y"), "%m-%Y")="${month}")
            and STR_TO_DATE(pst_month, "%m-%Y") <= STR_TO_DATE("${month}", "%m-%Y")
            UNION 
            select * from payment_student_table 
            INNER JOIN student_tab
            on payment_student_table.pst_student_id = student_tab.st_id
            INNER JOIN teacher_tab
            on payment_student_table.pst_teacher_id = teacher_tab.tt_id
            INNER JOIN student_course_tab
            on payment_student_table.pst_item_id = student_course_tab.ct_id
            INNER JOIN subscription_student_course_tab
            on payment_student_table.pst_item_subcription_id = subscription_student_course_tab.ssct_id
            INNER JOIN tuition_fee_tab
            on subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
            INNER JOIN student_program_lvl
            on tuition_fee_tab.tft_program_lvl = student_program_lvl.spl_id
            where pst_teacher_id=${teacherByUuid.tt_id}
            and STR_TO_DATE(pst_month, "%m-%Y") > STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")
            and pst_month = "${month}") t
            ORDER BY spl_name, ct_subject ASC;`);
        }
        let teacherSalaryDetails = [];
        if (teacherSalary!=null && !isArray(teacherSalary)){
            teacherSalaryDetails.push(teacherSalary);
        } else if (teacherSalary!=null) {
            teacherSalaryDetails = [...teacherSalary];
        }
        return teacherSalaryDetails;
    },

    // Get salary based on subscription
    // getTeachersSalary : async({month}) => {
    //     let teacherSalary = await queryOrUpdateDB(`select ssct_teacher_id as id,
    //         tt_uuid as uuid,
    //         tt_name as name, tt_ic as icNo, tt_phone_no as phoneNo,
    //         tt_payment_rate as paymentRate,
    //         CONVERT(sum(ssct_item_fee) * tt_payment_rate, INT) as totalSalary
    //         from subscription_student_course_tab
    //         INNER JOIN teacher_tab
    //         on subscription_student_course_tab.ssct_teacher_id = teacher_tab.tt_id
    //         where ((STR_TO_DATE("${month}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y")
    //         and STR_TO_DATE(ssct_expiry_date, "%m-%Y")) or ISNULL(ssct_expiry_date) or ssct_expiry_date = "")
    //         and ssct_active="${SUBSCRIPTION_ACTIVE}"
    //         group by ssct_teacher_id;`);
    //     let teacherSalariesArray = [];
    //     if (!isArray(teacherSalary)){
    //         teacherSalariesArray.push(teacherSalary);
    //     } else if (teacherSalary!=null){
    //         teacherSalariesArray =  [...teacherSalary];
    //     }
    //
    //     for (let i = 0; i < teacherSalariesArray.length; i ++){
    //         let teacherPayment = await queryOrUpdateDB(`select * FROM
    //             payment_teacher_table
    //             where ptt_teacher_id=${teacherSalariesArray[i].id}
    //             and ptt_month="${month}"`);
    //         if (teacherPayment == null){
    //             teacherSalariesArray[i].status = PAYMENT_STATUS_TEACHER_NEW;
    //         } else if (teacherSalariesArray[i].totalSalary !== Math.round(teacherPayment.ptt_total_amount * teacherSalariesArray[i].paymentRate)){
    //             teacherSalariesArray[i].totalPayment = Math.round(teacherPayment.ptt_total_amount * teacherSalariesArray[i].paymentRate);
    //             teacherSalariesArray[i].status = PAYMENT_STATUS_TEACHER_NOT_TALLY;
    //         } else {
    //             teacherSalariesArray[i].status = PAYMENT_STATUS_TEACHER_PAID;
    //             teacherSalariesArray[i].transactionDate = moment(teacherPayment.ptt_updated_date)
    //                 .format(DATE_FORMAT_RESPONSE);
    //         }
    //     }
    //
    //     return teacherSalariesArray;
    // },
    //
    // getTeacherSalaryDetail : async({month, teacherUuid}, teacherByUuid) => {
    //     let teacherSalary = await queryOrUpdateDB(`select
    //         ssct_id as subscriptionId,
    //         st_name as studentName, spl_name as programLvl, ssct_item_fee as fee,
    //         ct_id as subjectId, ct_subject as subjectName
    //         from subscription_student_course_tab
    //         INNER JOIN student_tab
    //         on subscription_student_course_tab.ssct_student_id = student_tab.st_id
    //         INNER JOIN teacher_tab
    //         on subscription_student_course_tab.ssct_teacher_id = teacher_tab.tt_id
    //         INNER JOIN tuition_fee_tab
    //         on subscription_student_course_tab.ssct_item_id = tuition_fee_tab.tft_id
    //         INNER JOIN student_course_tab
    //         on tuition_fee_tab.tft_course_id = student_course_tab.ct_id
    //         INNER JOIN student_program_lvl
    //         on tuition_fee_tab.tft_program_lvl = student_program_lvl.spl_id
    //         where ((STR_TO_DATE("${month}", "%m-%Y") BETWEEN STR_TO_DATE(ssct_effective_date, "%m-%Y")
    //         and STR_TO_DATE(ssct_expiry_date, "%m-%Y")) or ISNULL(ssct_expiry_date) or ssct_expiry_date = "")
    //         and ssct_active="${SUBSCRIPTION_ACTIVE}"
    //         and ssct_teacher_id=${teacherByUuid.tt_id}
    //         ORDER BY spl_name, ct_subject ASC;` );
    //     let teacherSalaryDetails = [];
    //     if (teacherSalary!=null && !isArray(teacherSalary)){
    //         teacherSalaryDetails.push(teacherSalary);
    //     } else if (teacherSalary!=null) {
    //         teacherSalaryDetails = [...teacherSalary];
    //     }
    //     return teacherSalaryDetails;
    // },

    getStudentPaymentHistory: async(uuid) => {
        const paymentHistory  = await queryOrUpdateDB(
            `SELECT pst_item_name as name, pst_transaction_date as transactionDate, 
                pst_created_date as dateCreated, pst_total_amount as fee
                FROM qms.payment_student_table 
                inner join student_tab
				on student_tab.st_id = payment_student_table.pst_student_id
                where st_uuid= "${uuid}"`);
        let paymentHistoryArray = [];
        if (paymentHistory!=null && !isArray(paymentHistory)){
            paymentHistoryArray.push(paymentHistory);
        } else if (paymentHistory!=null) {
            paymentHistoryArray = [...paymentHistory];
        }
        return paymentHistoryArray;
    },

    getTeacherPaymentHistory: async(uuid) => {
        const paymentHistory  = await queryOrUpdateDB(
            `SELECT ptt_item_type as name, ptt_month as transactionDate, 
                ptt_created_date as dateCreated, ptt_total_amount as fee
                FROM qms.payment_teacher_table 
                inner join teacher_tab
				on teacher_tab.tt_id = payment_teacher_table.ptt_teacher_id
                where tt_uuid= "${uuid}"`);
        let paymentHistoryArray = [];
        if (paymentHistory!=null && !isArray(paymentHistory)){
            paymentHistoryArray.push(paymentHistory);
        } else if (paymentHistory!=null) {
            paymentHistoryArray = [...paymentHistory];
        }
        return paymentHistoryArray;
    },

    getListOfBanks: async() => {
        return await queryOrUpdateDB(`select
            bt_name as name, bt_swift_code as swiftCode from bank_tab`);
    },

    getTransactionsByMonthAndProgramLevel: async(year, month, programLvl) => {
         const courses = await queryOrUpdateDB(`SELECT 
                pst_student_id as studentId,
                pst_student_name as studentName, 
                GROUP_CONCAT(ct_shortform SEPARATOR ', ') as courses,
                pst_transaction_type as transactionType,
                sum(pst_total_amount) as totalAmount,
                pst_transaction_date as transactionDate,
                pst_transaction_type as transactionType
                FROM qms.payment_student_table 
                inner join student_tab
                on pst_student_id = student_tab.st_id
                inner join student_course_tab
                on pst_item_id = student_course_tab.ct_id
                inner join qms.subscription_student_course_tab
                on pst_item_subcription_id = qms.subscription_student_course_tab.ssct_id
                inner join qms.tuition_fee_tab
                on qms.tuition_fee_tab.tft_id = qms.subscription_student_course_tab.ssct_item_id
                where YEAR(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${year} 
                and MONTH(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${month} 
                and (st_program_lvl = ${programLvl}) 
                group by st_id
                order by pst_student_name;`);

        const misc = await queryOrUpdateDB(`SELECT 
                pst_student_id as studentId,
                pst_student_name as studentName, 
                GROUP_CONCAT(mt_shortform SEPARATOR ', ') as miscs,
                pst_transaction_type as transactionType,
                sum(pst_total_amount) as totalAmount,
                pst_transaction_date as transactionDate,
                pst_transaction_type as transactionType
                FROM qms.payment_student_table 
                inner join student_tab
                on pst_student_id = student_tab.st_id
                inner join miscellaneous_tab
                on miscellaneous_tab.mt_id = payment_student_table.pst_item_id
                where YEAR(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${year} 
                and MONTH(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${month}
                and (st_program_lvl = ${programLvl})
                and pst_item_type = "${PAYMENT_ITEM_TYPE_MISC}"
                group by pst_student_id
                order by pst_student_name;`);

        let coursesArray = [];
        if (courses!=null && !isArray(courses)){
            coursesArray.push(courses);
        } else if (courses!=null){
            coursesArray =  [...courses];
        }

        let miscArray = [];
        if (misc!=null && !isArray(misc)){
            miscArray.push(misc);
        } else if (misc!=null){
            miscArray =  [...misc];
        }

        for (let i = 0; i < coursesArray.length; i ++){
            let courseItem = coursesArray[i];
            let miscItem = _.filter(miscArray, {studentId : courseItem.studentId});
            if (miscItem!=null && miscItem.length > 0) {
                courseItem.miscs = miscItem[0].miscs;
                courseItem.totalAmount = courseItem.totalAmount +  miscItem[0].totalAmount;
            }
        }

        for (let i = 0; i < miscArray.length; i ++){
            let miscItem = miscArray[i];
            let courseItem = _.filter(coursesArray, {studentId : miscItem.studentId});
            if (courseItem.length === 0) {
                coursesArray.push(miscItem)
            }
        }

        // Use of _.sortBy() method
        let sortedCoursesArray = _.sortBy(coursesArray,
            [function(o) { return o.studentName; }]);

        return sortedCoursesArray;

    },

    getTransactionsByMonth: async(year, month) => {
        // const transactions = await queryOrUpdateDB(`SELECT
        //         sum(pst_total_amount) as totalAmount,
        //         pst_month as tuitionMonth,
        //         pst_transaction_date as transactionDate
        //         FROM qms.payment_student_table
        //         inner join student_tab
        //         on pst_student_id = student_tab.st_id
        //         where YEAR(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = 2021
        //         and MONTH(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = 8
        //         order by pst_student_name;`);

        const yearRequest = moment(year, "YYYY").format("YYYY")
        const monthRequest = moment(month, "M").format("MM")
        const defaultResult = {
            totalAmount : 0,
            transactionDate : "01-" + monthRequest + "-" + yearRequest
        }
        let transactionsByBank = await queryOrUpdateDB(`SELECT 
                sum(pst_total_amount) as totalAmount,
                pst_transaction_date as transactionDate
                FROM qms.payment_student_table 
                inner join student_tab
                on pst_student_id = student_tab.st_id
                where YEAR(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${year}  
                and MONTH(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${month}
                and pst_transaction_type = "BANK"
                order by pst_student_name;`);

        if (transactionsByBank.totalAmount == null){
            transactionsByBank = defaultResult;
        }

        let transactionsByCash = await queryOrUpdateDB(`SELECT 
                sum(pst_total_amount) as totalAmount,
                pst_transaction_date as transactionDate
                FROM qms.payment_student_table 
                inner join student_tab
                on pst_student_id = student_tab.st_id
                where YEAR(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${year} 
                and MONTH(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${month}
                and pst_transaction_type = "CASH"
                order by pst_student_name;`);
        if (transactionsByCash.totalAmount == null){
            transactionsByCash = defaultResult;
        }

        let transactionsUnknownType = await queryOrUpdateDB(`SELECT 
                sum(pst_total_amount) as totalAmount,
                pst_transaction_date as transactionDate
                FROM qms.payment_student_table 
                inner join student_tab
                on pst_student_id = student_tab.st_id
                where YEAR(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${year}  
                and MONTH(STR_TO_DATE(pst_transaction_date, "%d-%m-%Y")) = ${month}
                and (ISNULL(pst_transaction_type = "BANK") or pst_transaction_type="")
                order by pst_student_name;`);

        if (transactionsUnknownType.totalAmount == null){
            transactionsUnknownType = defaultResult;
        }
        //test
        const total = {
            totalAmount :transactionsByBank.totalAmount + transactionsByCash.totalAmount + transactionsUnknownType.totalAmount,
            transactionDate : transactionsByBank.transactionDate
        }

        return {
            bank: transactionsByBank,
            cash: transactionsByCash,
            unknown: transactionsUnknownType,
            total
        }

    },
};
