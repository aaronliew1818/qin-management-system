import {insertDB, queryOrUpdateDB} from "../../services/db";


function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

export const quizDao = {

    getAllQuiz: async() => {
        // "at_id": 104,
        //     "at_author_name": "Admin",
        //     "at_author_id": null,
        //     "at_content": "一二三四五",
        //     "at_images": null,
        //     "at_status": "ACTIVE",
        //     "at_created_date": "2022-08-10T23:47:44.000Z",
        //     "at_updated_date": "2022-08-10T23:47:44.000Z"
        const quiz = await queryOrUpdateDB(`
            select qt_id as id,
                   qt_tuition_id as tuitionId,
                   qt_form_id as formId,
                   qt_title as title,
                   qt_url as url,
                   qt_status as status,
                   qt_created_date as recordCreateDate 
            from quiz_tab where qt_status = 'ACTIVE' order by qt_created_date desc
          `);
        if (quiz == null) {
            return [];
        } else if (!isArray(quiz)){
            return [quiz]
        }
        return quiz;
    },

    createQuiz: async(quiz) => {
        return await insertDB(`INSERT INTO quiz_tab
                                   (qt_tuition_id,qt_form_id,qt_title, qt_url,qt_status,qt_created_date,qt_updated_date)
                               VALUES (?, ?, ?, ?, ?, now(), now())`, quiz);
    },

    updateQuizById: async({quizId, tuitionId, formId, title, url, status}) => {
        return await queryOrUpdateDB(
            `UPDATE quiz_tab 
                    set qt_tuition_id = COALESCE(${tuitionId ? `"${tuitionId}"` : null},qt_tuition_id),
                        qt_form_id = COALESCE(${formId ? `"${formId}"` : null},qt_form_id),
                        qt_url = COALESCE(${url ? `"${url}"` : null},qt_url),
                        qt_title = COALESCE(${title ? `"${title}"` : null},qt_title),
                        qt_status = COALESCE(${status ? `"${status}"` : null},qt_status),
                        qt_updated_date = now() where qt_id = "${quizId}"`);
    },
};


