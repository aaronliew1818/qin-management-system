import {insertDB, queryOrUpdateDB} from "../../services/db";
import _ from 'lodash';
import {SUBSCRIPTION_CANCELLED, SUBSCRIPTION_ACTIVE, TUITION_FEE_STATUS_ACTIVE} from ".././dao/constants";
import moment from "moment";

export const courseDao = {

    getCourseByName: async(idNo) => {
        return await queryOrUpdateDB(`select
            * from student_tab
            where st_ic_no = "${idNo}"
            limit 1
          `);
    },

    getCourseById: async(id) => {
        return await queryOrUpdateDB(`select
            * from student_course_tab
            where ct_id = "${id}"
          `);
    },

    insertCourse:  async(data) => {
        return await insertDB(`INSERT INTO student_course_tab 
          ( ct_subject, ct_created_date, ct_updated_date )
          VALUES ( ?, now(), now())`, data);
    },

    getCourseSubscriptionById: async(subscriptionId) => {
        return await queryOrUpdateDB(`select
            * from subscription_student_course_tab 
            where ssct_id = "${subscriptionId}" and
            ssct_active = "${SUBSCRIPTION_ACTIVE}"
        `);
    },

    cancelCourseSubscriptionById: async(subscriptionId, cancellationDate, status = SUBSCRIPTION_CANCELLED ) => {
        const expiryDate = moment(cancellationDate).format("MM-YYYY")
        return await queryOrUpdateDB(`update subscription_student_course_tab set
            ssct_active = "${status}", 
            ssct_expiry_date="${expiryDate}", ssct_updated_date=now() 
            where ssct_id = ${subscriptionId}
        `);
    },

    cancelCourseSubscriptionByTuitionId: async(tuitionId) => {
        return await queryOrUpdateDB(`update subscription_student_course_tab set
            ssct_active = "${SUBSCRIPTION_CANCELLED}", ssct_updated_date=now() 
            where ssct_item_id = ${tuitionId}
        `);
    },


    getMiscSubscriptionById: async(subscriptionId) => {
        return await queryOrUpdateDB(`select
            * from subscription_student_misc_table 
            where ssmt_id = "${subscriptionId}" and
            ssmt_active = "${SUBSCRIPTION_ACTIVE}"
        `);
    },

    cancelMiscSubscriptionById: async(subscriptionId, cancellationDate, status = SUBSCRIPTION_CANCELLED ) => {
        const expiryDate = moment(cancellationDate).format("MM-YYYY")
        return await queryOrUpdateDB(`update subscription_student_misc_table set
            ssmt_active = "${status}", 
            ssmt_expiry_date="${expiryDate}",
            ssmt_updated_date=now() 
            where ssmt_id = ${subscriptionId}
        `);
    },

    checkIfStudentSubscribeTheCourse: async(data) => {
        return await queryOrUpdateDB(`select
            * from subscription_student_course_tab 
            where ssct_student_id = "${data.studentId}" and 
            ssct_teacher_id = "${data.teacherId}" and
            ssct_item_id = "${data.itemId}" and
            ssct_active = "${SUBSCRIPTION_ACTIVE}"
        `);
    },

    subscribeStudentCourse: async(data) => {
        return await insertDB(`INSERT INTO subscription_student_course_tab 
          ( ssct_student_id, ssct_student_name, ssct_teacher_id, ssct_teacher_name, ssct_item_type, 
          ssct_item_fee, ssct_item_id, ssct_active, ssct_recurrence_type, ssct_recurrence_duration, 
          ssct_effective_date, ssct_created_date, ssct_updated_date)
          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())`, data);
    },

    updateStudentCourse: async({subscriptionId, studentId, studentName, teacherId, teacherName, itemType, tuitionFee,
                                   tuitionId, subscriptionStatus, recurrenceType, recurrenceDuration}) => {
        return await queryOrUpdateDB(`UPDATE subscription_student_course_tab SET
          ssct_student_id = ${studentId}, ssct_student_name = "${studentName}", 
          ssct_teacher_id = ${teacherId}, ssct_teacher_name = "${teacherName}", ssct_item_type = "${itemType}", 
          ssct_item_fee = ${tuitionFee}, ssct_item_id = ${tuitionId}, 
          ssct_active = "${subscriptionStatus}", ssct_recurrence_type = "${recurrenceType}", 
          ssct_recurrence_duration = ${recurrenceDuration}, 
          ssct_updated_date = now() where ssct_id = ${subscriptionId}`);
    },

    checkIfStudentSubscribeTheMisc: async(data) => {
        return await queryOrUpdateDB(`select
            * from subscription_student_misc_table 
            where ssmt_student_id = "${data.studentId}" and 
            ssmt_item_id = "${data.itemId}" and
            ssmt_active = "${SUBSCRIPTION_ACTIVE}"
        `);
    },

    subscribeMisc: async(data) => {
        return await insertDB(`INSERT INTO subscription_student_misc_table 
          (ssmt_student_id, ssmt_student_name, ssmt_item_id, ssmt_item_name, ssmt_fee, ssmt_active, ssmt_recurrence_type, ssmt_recurrence_duration, ssmt_effective_date, ssmt_created_date, ssmt_updated_date)
          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())`, data);
    },

    updateStudentMisc: async({subscriptionId, studentId, studentName, miscId, miscName, miscFee,
                                 subscriptionStatus, recurrenceType, recurrenceDuration}) => {
        return await queryOrUpdateDB(`UPDATE subscription_student_misc_table SET
          ssmt_student_id = ${studentId}, ssmt_student_name = "${studentName}", 
          ssmt_fee = ${miscFee}, ssmt_item_id = ${miscId}, ssmt_item_name = "${miscName}", 
          ssmt_active = "${subscriptionStatus}", ssmt_recurrence_type = "${recurrenceType}", 
          ssmt_recurrence_duration = ${recurrenceDuration}, 
          ssmt_updated_date = now() where ssmt_id = ${subscriptionId}`);
    },

    getAllCourses: async() => {
        return await queryOrUpdateDB(`select
            ct_id as id, ct_subject as name from student_course_tab`);
    },

    getProgramLvl: async() => {
        return await queryOrUpdateDB(`select
            spl_id as id, spl_name as name from student_program_lvl`);
    },

    getAllTuitionFees: async() => {
        // tft_id, tft_teacher_id, tft_course_id, tft_program_lvl, tft_fee, tft_created_date, tft_updated_date
        let tuitionFees = await queryOrUpdateDB(`select
            tuition_fee_tab.tft_id as id, tuition_fee_tab.tft_teacher_id as teacherId, student_course_tab.ct_id as courseId, 
            student_course_tab.ct_subject as subjectName, tuition_fee_tab.tft_program_lvl as programLvl, 
            tuition_fee_tab.tft_fee as fee
            from tuition_fee_tab
            left join student_course_tab on tuition_fee_tab.tft_course_id = student_course_tab.ct_id
            where tft_status="${TUITION_FEE_STATUS_ACTIVE}"`);

        if (!Array.isArray(tuitionFees)) {
            tuitionFees = [{ ...tuitionFees}];
        }

        return tuitionFees;
    },

    getTuitionFeeByTeacherIdAndCourseIdAndProgramLvl: async(teacherId, programLvl, subjectId) => {
        // tft_id, tft_teacher_id, tft_course_id, tft_program_lvl, tft_fee, tft_created_date, tft_updated_date
        let tuitionFees = await queryOrUpdateDB(`select
            * from tuition_fee_tab where tft_teacher_id=${teacherId} 
            and tft_course_id=${subjectId} and tft_program_lvl = ${programLvl} and tft_status="${TUITION_FEE_STATUS_ACTIVE}"`);

        return tuitionFees;
    },

    getTuitionFeeById: async(tuitionId) => {
        // tft_id, tft_teacher_id, tft_course_id, tft_program_lvl, tft_fee, tft_created_date, tft_updated_date
        let tuitionFee = await queryOrUpdateDB(`select
            * from tuition_fee_tab where tft_id=${tuitionId}`);
        return tuitionFee;
    },
};
