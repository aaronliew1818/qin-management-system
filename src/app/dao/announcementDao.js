import {insertDB, queryOrUpdateDB} from "../../services/db";


function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

export const announcementDao = {

    getAllAnnouncements: async() => {
        // "at_id": 104,
        //     "at_author_name": "Admin",
        //     "at_author_id": null,
        //     "at_content": "一二三四五",
        //     "at_images": null,
        //     "at_status": "ACTIVE",
        //     "at_created_date": "2022-08-10T23:47:44.000Z",
        //     "at_updated_date": "2022-08-10T23:47:44.000Z"
        const announcements = await queryOrUpdateDB(`
            select at_id as id,
                   at_author_name as authorName, at_content as content,
                   at_status as status,
                   at_program_level as programLvl, 
                   at_course_id as subjectId,
                   at_class_session as schedule,
                   at_created_date as recordCreateDate 
            from announcement_tab where at_status = 'ACTIVE' order by at_created_date desc
          `);
        if (announcements == null) {
            return [];
        } else if (!isArray(announcements)){
            return [announcements]
        }
        return announcements;
    },

    createAnnouncement: async(announcement) => {
        return await insertDB(`INSERT INTO announcement_tab
                                   ( at_author_name, at_program_level, at_course_id,at_class_session, at_content,at_status,at_created_date,at_updated_date )
                               VALUES (?, ?, ?, ?, ?, ?, now(), now())`, announcement);
    },

    updateAnnouncement: async({id, authorName, content, subjectId, programLvl, classSession, status}) => {
        return await queryOrUpdateDB(
            `UPDATE announcement_tab 
                    set at_author_name = COALESCE(${authorName ? `"${authorName}"` : null},at_author_name),
                    at_content = COALESCE(${content ? `"${content}"` : null},at_content),
                    at_program_level = COALESCE(${programLvl ? `"${programLvl}"` : null},at_program_level),
                    at_course_id = COALESCE(${subjectId ? `"${subjectId}"` : null},at_course_id),
                    at_class_session = COALESCE(${classSession ? `"${classSession}"` : null},at_class_session),
                    at_status = COALESCE(${status ? `"${status}"` : null},at_status),
                    at_updated_date = now() where at_id = "${id}"`);
    },
};


