import {insertDB, queryOrUpdateDB} from "../../services/db";
import {TUITION_FEE_STATUS_ACTIVE, TUITION_FEE_STATUS_CANCELLED, SUBSCRIPTION_ACTIVE, SUBSCRIPTION_CANCELLED} from "./constants";

export const teacherDao = {
    getTeachers: async({name}) => {
        let searchQuery = "";
        if (name != null && name.length > 0){
            name = decodeURIComponent(name);
            console.log(name);
            if (searchQuery.length > 0){
                searchQuery += ` and tt_name LIKE "%${name}%"`
            } else {
                searchQuery += `where tt_name LIKE "%${name}%"`
            }
        }

        let teacher = await queryOrUpdateDB(`select
            tt_id as id, tt_name as name, tt_ic as icNo, tt_phone_no as phoneNo, 
            tt_gender as gender, tt_uuid as uuid from teacher_tab ${searchQuery}`);

        if (!Array.isArray(teacher)) {
            teacher = [{ ...teacher}];
        }
        return teacher;
    },

    getSingleTeacherByUuid: async({uuid}) => {
        // tuitionCourse: [ { teacher : "", programLvl: "", subjectId: "", fee: 0 } ],
        //     misc: [ { id : "", name: "", fee: 0 } ],

        let teacher = await queryOrUpdateDB(`select
            tt_id as id, tt_name as name, tt_ic as icNo, tt_phone_no as phoneNo, 
            tt_gender as gender, tt_uuid as uuid, tt_payment_type as paymentType, tt_payment_rate as paymentRate,
            tt_bank_name as bankName, tt_bank_account_no as bankAccountNo
            from teacher_tab where tt_uuid = "${uuid}"`);


        let tuitionCourses = await queryOrUpdateDB(`select
            tft_id as id, tft_program_lvl as programLvl, 
            tft_course_id as subjectId, tft_course_name as subjectName, tft_fee as fee
            from tuition_fee_tab where tft_teacher_id = ${teacher.id} and tft_status = "${TUITION_FEE_STATUS_ACTIVE}"`);


        if (!Array.isArray(tuitionCourses) && tuitionCourses!=null) {
            tuitionCourses = [{...tuitionCourses}];
        } else if (tuitionCourses == null){
            tuitionCourses = [];
        }

        return {
            uuid,
            name: teacher.name,
            nric: teacher.icNo,
            dob: teacher.dob,
            phoneNo: teacher.phoneNo ? teacher.phoneNo : "",
            email: teacher.email ? teacher.email : "",
            gender: teacher.gender,
            paymentType: teacher.paymentType,
            paymentRate: teacher.paymentRate,
            bankName: teacher.bankName,
            bankAccountNo: teacher.bankAccountNo,
            tuitionCourses,
        };
    },

    getTeacherInfoById: async(id) => {
        return await queryOrUpdateDB(`select
            * from teacher_tab
            where tt_id = "${id}"
            limit 1
          `);
    },

    getTeacherInfoByUuid: async(uuid) => {
        return await queryOrUpdateDB(`select
            * from teacher_tab
            where tt_uuid = "${uuid}"
            limit 1
          `);
    },
    insertTeacherInfo:  async(data) => {
        return await insertDB(`INSERT INTO teacher_tab 
          ( tt_name, tt_ic, tt_phone_no, tt_gender, tt_payment_type, tt_payment_rate, tt_bank_name, 
          tt_bank_account_no, tt_uuid, tt_created_date, tt_updated_date)
          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, uuid(), now(), now())`, data);
    },

    updateTeacherInfo:  async({name, identification, gender, phoneNo, paymentType, paymentRate, bankName, bankAccountNo, uuid}) => {
        return await queryOrUpdateDB(`UPDATE teacher_tab 
            SET tt_name = "${name}", 
            tt_ic = "${identification}", 
            tt_gender = "${gender}", 
            tt_phone_no = "${phoneNo}", 
            tt_payment_type = "${paymentType}", 
            tt_payment_rate = "${paymentRate}", 
            tt_bank_name = "${bankName}", 
            tt_bank_account_no = "${bankAccountNo}", 
            tt_updated_date = now() where tt_uuid = "${uuid}"`);
    },

    insertTuitionFees:  async(data) => {
        return await insertDB(`INSERT INTO tuition_fee_tab 
          ( tft_teacher_id, tft_teacher_name, tft_course_id, tft_course_name, tft_program_lvl, tft_fee, tft_status, tft_created_date, tft_updated_date )
          VALUES ( ?, ?, ?, ?, ?, ?, ?, now(), now())`, data);
    },

    getTuitionFeeById: async(tuitionId) => {
        return await queryOrUpdateDB(`select
            * from tuition_fee_tab
            where tft_id = "${tuitionId}" and tft_status = "${TUITION_FEE_STATUS_ACTIVE}"
          `);
    },

    cancelTuitionFeeById:  async(tuitionId) => {
        return await queryOrUpdateDB(`update tuition_fee_tab set
            tft_status = "${TUITION_FEE_STATUS_CANCELLED}", tft_updated_date=now() 
            where tft_id = ${tuitionId}
        `);
    },

    updateTuitionFees:  async({tuitionId, teacherName, courseId, courseName, programLvl,
                                  tuitionType, fee}) => {
        return await queryOrUpdateDB(`UPDATE tuition_fee_tab 
            SET tft_teacher_name = "${teacherName}", 
            tft_course_id = "${courseId}", 
            tft_course_name = "${courseName}", 
            tft_program_lvl = "${programLvl}", 
            tft_fee = "${fee}",
            tft_updated_date = now() where tft_id = "${tuitionId}"`);
    },

    updateSubscriptionInfoByTuitionId:  async({tuitionId, teacherName, fee, tuitionType}) => {
        return await queryOrUpdateDB(`UPDATE subscription_student_course_tab 
            SET ssct_teacher_name = "${teacherName}",
            ssct_item_fee = "${fee}", 
            ssct_item_type = "${tuitionType}",
            ssct_updated_date = now() where ssct_item_id = ${tuitionId} 
            and ssct_active="${SUBSCRIPTION_ACTIVE}"`);
    },

    getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse: async(programLvl, teacherId, courseId) => {
        return await queryOrUpdateDB(`select
            * from tuition_fee_tab
            where tft_program_lvl = "${programLvl}" and
            tft_teacher_id = "${teacherId}" and  
            tft_course_id = "${courseId}" and tft_status = "${TUITION_FEE_STATUS_ACTIVE}"
            limit 1
          `);
    },
};
