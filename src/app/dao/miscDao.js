import {insertDB, queryOrUpdateDB} from "../../services/db";
import _ from 'lodash';

export const miscDao = {
    getAllMisc: async() => {
        return await queryOrUpdateDB(`select
           mt_id as id, mt_name as name, mt_fee as fee from miscellaneous_tab`);
    },

    getMiscById: async(id) => {
        return await queryOrUpdateDB(`select
           * from miscellaneous_tab where mt_id="${id}"`);
    },

    getMiscSubscriptionInfoById: async(id) => {
        return await queryOrUpdateDB(`select
            * from subscription_student_misc_table
            where ssmt_id = "${id}"
            limit 1
          `);
    },

};