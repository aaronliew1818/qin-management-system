import {insertDB, queryOrUpdateDB} from "../../services/db";


function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

export const timetableDao = {

    getTimetables: async() => {
        // "at_id": 104,
        //     "at_author_name": "Admin",
        //     "at_author_id": null,
        //     "at_content": "一二三四五",
        //     "at_images": null,
        //     "at_status": "ACTIVE",
        //     "at_created_date": "2022-08-10T23:47:44.000Z",
        //     "at_updated_date": "2022-08-10T23:47:44.000Z"
        const timetable = await queryOrUpdateDB(`
            select tst_id as id,
                   tst_subject_id as subjectId,
                   tst_subject_name as subjectName,
                   tst_program_level as programLvl,
                   spl_name as programLvlLabel,
                   tst_start as startTime,
                   tst_end as endTime,
                   tst_repeat_day as dayOfWeek,
                   tst_duration as duration,
                   tst_status as status,
                   tst_classroom as classroom,
                   tst_ampm_session as classSession,
                   tst_created_date as recordCreateDate,
                   tst_updated_date as recordUpdateDate
            from tuition_schedule_tab 
            inner join student_program_lvl 
            on spl_id = tst_program_level 
            where tst_status = 'ACTIVE' order by tst_repeat_day, tst_start
          `);
        if (timetable == null) {
            return [];
        } else if (!isArray(timetable)){
            return [timetable]
        }
        return timetable;
    },

    updateTimetableById: async({timetableId, startTime, endTime, dayOfWeek,  subjectId, subjectName, programLvl, classroom, classSession, status}) => {
        return await queryOrUpdateDB(
            `UPDATE tuition_schedule_tab 
                    set tst_start = COALESCE(${startTime ? `"${startTime}"` : null},tst_start),
                        tst_end = COALESCE(${endTime ? `"${endTime}"` : null},tst_end),
                        tst_repeat_day = COALESCE(${dayOfWeek ? `"${dayOfWeek}"` : null},tst_repeat_day),
                        tst_subject_id = COALESCE(${subjectId ? `"${subjectId}"` : null},tst_subject_id),
                        tst_subject_name = COALESCE(${subjectName ? `"${subjectName}"` : null},tst_subject_name),
                        tst_program_level = COALESCE(${programLvl ? `"${programLvl}"` : null},tst_program_level),
                        tst_classroom = COALESCE(${classroom ? `"${classroom}"` : null},tst_classroom),
                        tst_ampm_session = COALESCE(${classSession ? `"${classSession}"` : null},tst_ampm_session),
                        tst_status = COALESCE(${status ? `"${status}"` : null},tst_status),
                        tst_updated_date = now() where tst_id = "${timetableId}"`);
    },

    createTimetable: async(timetable) => {
        return await insertDB(`INSERT INTO tuition_schedule_tab
                                   (tst_subject_id,tst_subject_name,tst_program_level,tst_start,tst_end,tst_repeat_day,tst_classroom, tst_ampm_session, tst_duration,tst_status,tst_created_date,tst_updated_date)
                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())`, timetable);
    },
};


