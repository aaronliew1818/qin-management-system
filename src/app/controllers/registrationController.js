import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import config from '../../config';
import geHeader from '../../utils/geHeader';
import { sendErrorToSlackChannel } from '../../services/slack';;
import { queryOrUpdateDB, insertDB, insertGEResponse, getCProtectPaymentStatusWithMsisdn, insertValidationError } from '../../services/db';
import { generateAuthToken, encryptPaymentToken, decryptPaymentToken, generateHMAC, generateApiSecretKey } from '../../utils/crypto';
import { toCent, toDollarDisplay, calTotalAmount, generateUniqueId, generateEcertId, generateTakafulEcertId, addPrefixInsuredPersonListData, convertGeErrosMessages, getInternationalNumber } from '../../utils';
import { authDao } from './../dao/authDao'
import { courseDao } from './../dao/courseDao'
import { miscDao } from './../dao/miscDao'
import {studentDao} from "../dao/studentDao";
import {teacherDao} from "../dao/teacherDao";
import {
  ITEM_TYPE_TUITION,
  SUBSCRIPTION_ACTIVE,
  RECURRENCE_TYPE_MONTH,
  ITEM_TYPE_DAYCARE,
  TUITION_FEE_STATUS_ACTIVE,
  SUBSCRIPTION_CANCELLED,
  SUBSCRIPTION_DELETED,
  MORNING_SCHEDULE,
  AFTERNOON_SCHEDULE,
  DAYCARE_COURSE_ID
} from "../dao/constants";

export const registrationController = {
  registerStudent: async (req, res) => {
    const studentInfoByIdNo = await studentDao.getStudentInfoByIdNo(req.body.nric);
    console.log(req.body.schedule);
    if (req.body.schedule !== MORNING_SCHEDULE && req.body.schedule !== AFTERNOON_SCHEDULE) {
      return res.status(404).json({ error: { message: 'Please select a valid schedule' } });
    }

    if (studentInfoByIdNo != null) {
      return res.status(404).json({ error: { message: 'Student is already existed in our database' } });
    }

    const enrolmentDate = moment(req.body.enrolmentDate, "MM-YYYY").format("YYYY-MM-DD HH:mm:ss")
    const studentInfo = [
      req.body.name,
      req.body.nric,
      req.body.gender,
      req.body.phoneNo,
      req.body.email,
      req.body.programLvl,
      req.body.emergencyContactName,
      req.body.emergencyContactPhoneNo,
      req.body.emergencyContactRelationship,
      req.body.schedule,
      enrolmentDate
    ];

    const result = await studentDao.insertStudentInfo(studentInfo);
    const studentId = result.insertId;


    //subscribe student course
    const tuitionCourses = req.body.tuitionCourse;
    for (let i = 0; i < tuitionCourses.length; i++) {
      const course = tuitionCourses[i];
      if (course.teacher == null || course.teacher === "") continue;

      const teacherInfoById = await teacherDao.getTeacherInfoById(course.teacher);

      const checkIfStudentSubscribeTheCourseQuery = {
        studentId,
        teacherId: teacherInfoById.tt_id,
        itemId: course.subjectId,
      };
      const checkIfStudentSubscribeTheCourse = await courseDao.checkIfStudentSubscribeTheCourse(checkIfStudentSubscribeTheCourseQuery);

      if (checkIfStudentSubscribeTheCourse != null){
        console.log("Student already subscribe to this course. Skip this");
        return;
      }

      const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(course.programLvl, course.teacher, course.subjectId);
      let tuitionFee = course.fee;

      const tuitionFees = [
        studentId,
        req.body.name,
        teacherInfoById.tt_id,
        teacherInfoById.tt_name,
        ITEM_TYPE_TUITION, //TUITION , DAYCARE
        tuitionFee,
        tuitionFeeObj.tft_id, //serve as reference only. Will be removed
        SUBSCRIPTION_ACTIVE, // ACTIVE
        RECURRENCE_TYPE_MONTH, //MONTH, DAY
        1, //everymonth
        course.effectiveDate ? moment(course.effectiveDate, "MM-YYYY").format("MM-YYYY") : moment().format("MM-YYYY")
      ];

      await courseDao.subscribeStudentCourse(tuitionFees);
    }


    //TODO: NO MORE MISC SUBSCRIPTION TAB
    //subscribe misc
    const misc = req.body.misc;
    for (let i = 0; i < misc.length; i++) {
      const miscItem = misc[i];
      if (miscItem.id == null || miscItem.id === "") continue;

      const checkIfStudentSubscribeTheMiscQuery = {
        studentId,
        itemId: miscItem.id,
      };

      const checkIfStudentSubscribeTheMisc = await courseDao.checkIfStudentSubscribeTheMisc(checkIfStudentSubscribeTheMiscQuery);

      if (checkIfStudentSubscribeTheMisc > 0){
        console.log("Student already subscribe to this misc. Skip this");
        return;
      }

      const miscById = await miscDao.getMiscById(miscItem.id);
      const tuitionFees = [
        studentId,
        req.body.name,
        miscItem.id,
        miscById.mt_name,
        miscById.mt_fee,
        SUBSCRIPTION_ACTIVE, //0 = active
        RECURRENCE_TYPE_MONTH, //1 = month, 2 = days
        1, //everymonth,
        moment().format("MM-YYYY")
      ];

      await courseDao.subscribeMisc(tuitionFees);
    }

    //subscribe misc course

    return res.json({ data: { message: 'Student is created' } });
  },
  registerTeacher: async (req, res) =>{
    // const teacherInfoByIdNo = await teacherDao.getTeacherInfoByIdNo(req.body.identification);
    // if (teacherInfoByIdNo != null){
    //   return res.status(404).json({ error: { message: 'Teacher is already existed in our database' } });
    // }

    const teacherData = [
      req.body.name,
      req.body.identification,
      req.body.phoneNo,
      req.body.gender,
      req.body.paymentType,
      req.body.paymentRate,
      req.body.bankName,
      req.body.bankAccountNo
    ];
    const result = await teacherDao.insertTeacherInfo(teacherData);
    const teacherId = result.insertId;
    // tft_id, tft_teacher_id, tft_course_id, tft_program_lvl, tft_fee

    const tuitionCourses = req.body.tuitionCourse;
    for (let i = 0; i < tuitionCourses.length; i++) {
      const teacherName = req.body.name;
      const course = tuitionCourses[i];
      const courseObj = await courseDao.getCourseById(course.subjectId);
      console.log(courseObj);
      const tuitionFees = [
        teacherId,
        teacherName,
        courseObj.ct_id,
        courseObj.ct_subject,
        course.programLvl,
        course.fee,
        TUITION_FEE_STATUS_ACTIVE
      ];
      await teacherDao.insertTuitionFees(tuitionFees);
    }
    return res.json({ data: { message: 'Teacher info saved' } });
  },

  registerCourse: async (req, res) =>{
    const courseByName = await courseDao.getCourseByName(req.body.subject);
    if (courseByName != null){
      return res.status(404).json({ error: { message: 'Course is already existed in our database' } });
    }

    const data = [
      req.body.subjectName,
      req.body.programLvl,
    ];

    await courseDao.insertCourse(data);
    return res.json({ data: { message: 'Course is saved' } });
  },

  updateStudentStatus: async (req, res) => {
    let studentInfoByUuid;
    studentInfoByUuid = await studentDao.getStudentInfoByUuuid(req.body.uuid);
    if (studentInfoByUuid == null) {
      return res.status(404).json({ error: { message: "This student doesn't exist in our database"} });
    }
    await studentDao.updateStudentStatus(req.body);
    return res.json({ data: { message: "Student's status is successfully updated!" } });
  },

  updateStudentInfo: async (req, res) => {
    let studentInfoByUuid;
    studentInfoByUuid = await studentDao.getStudentInfoByUuuid(req.body.uuid);
    if (studentInfoByUuid == null) {
      return res.status(404).json({ error: { message: "This student doesn't exist in our database"} });
    }

    const studentId = studentInfoByUuid.st_id;
    await studentDao.updateStudentInfo(req.body);

    //subscribe student course
    const tuitionCourses = req.body.tuitionCourse;
    for (let i = 0; i < tuitionCourses.length; i++) {
      const course = tuitionCourses[i];

      if (course.subscriptionId !== null && course.subscriptionId>0){
        continue;
      }
      const teacherInfoById = await teacherDao.getTeacherInfoById(course.teacher);

      const tuitionFee = await courseDao.getTuitionFeeByTeacherIdAndCourseIdAndProgramLvl(teacherInfoById.tt_id, course.programLvl, course.subjectId)

      const checkIfStudentSubscribeTheCourseQuery = {
        studentId,
        teacherId: teacherInfoById.tt_id,
        itemId: tuitionFee.tft_id,
      };

      // not allow to student to update subscription info anymore
      const checkIfStudentSubscribeTheCourse = await courseDao.checkIfStudentSubscribeTheCourse(checkIfStudentSubscribeTheCourseQuery);
      if (checkIfStudentSubscribeTheCourse == null){
        const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(course.programLvl, course.teacher, course.subjectId);
        let tuitionFee = course.fee;

        const tuitionFees = [
          studentId,
          req.body.name,
          teacherInfoById.tt_id,
          teacherInfoById.tt_name,
          ITEM_TYPE_TUITION, //TUITION , DAYCARE
          tuitionFee,
          tuitionFeeObj.tft_id,
          SUBSCRIPTION_ACTIVE, // ACTIVE
          RECURRENCE_TYPE_MONTH, //MONTH, DAY
          1, //everymonth
          course.effectiveDate ? moment(course.effectiveDate, "MM-YYYY").format("MM-YYYY") : moment().format("MM-YYYY")
        ];

        await courseDao.subscribeStudentCourse(tuitionFees);
      } else {
        return res.status(404).json({ error: { message: "This student already subscribed this tuition course."} });
      }
    }
    //   if (checkIfStudentSubscribeTheCourse != null){
    //     const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(course.programLvl, course.teacher, course.subjectId);
    //
    //     const tuitionFees = {
    //       subscriptionId: checkIfStudentSubscribeTheCourse.ssct_id,
    //       studentId : studentId,
    //       studentName : req.body.name,
    //       teacherId: teacherInfoById.tt_id,
    //       teacherName: teacherInfoById.tt_name,
    //       itemType: ITEM_TYPE_TUITION, //TUITION , DAYCARE
    //       tuitionFee: tuitionFeeObj.tft_fee,
    //       tuitionId: tuitionFeeObj.tft_id,
    //       subscriptionStatus: SUBSCRIPTION_ACTIVE, // ACTIVE
    //       recurrenceType: RECURRENCE_TYPE_MONTH, //MONTH, DAY
    //       recurrenceDuration: 1, //everymonth
    //     };
    //     await courseDao.updateStudentCourse(tuitionFees);
    //   } else {
    //     const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(course.programLvl, course.teacher, course.subjectId);
    //     const tuitionFees = [
    //       studentId,
    //       req.body.name,
    //       teacherInfoById.tt_id,
    //       teacherInfoById.tt_name,
    //       ITEM_TYPE_TUITION, //TUITION , DAYCARE
    //       tuitionFeeObj.tft_fee,
    //       tuitionFeeObj.tft_id,
    //       SUBSCRIPTION_ACTIVE, // ACTIVE
    //       RECURRENCE_TYPE_MONTH, //MONTH, DAY
    //       1, //everymonth
    //       moment().format("MM-YYYY")
    //     ];
    //
    //     await courseDao.subscribeStudentCourse(tuitionFees);
    //   }
    // }

    //Handle misc subscription
    const misc = req.body.misc;
    for (let i = 0; i < misc.length; i++) {
      const miscItem = misc[i];

      if (miscItem.subscriptionId != null && miscItem.subscriptionId > 0) continue;

      const checkIfStudentSubscribeTheMiscQuery = {
        studentId,
        itemId: miscItem.id,
      };

      const checkIfStudentSubscribeTheMisc = await courseDao.checkIfStudentSubscribeTheMisc(checkIfStudentSubscribeTheMiscQuery);
      if (checkIfStudentSubscribeTheMisc == null){
        const miscById = await miscDao.getMiscById(miscItem.id);
        const miscSubscription = [
          studentId,
          req.body.name,
          miscItem.id,
          miscById.mt_name,
          miscById.mt_fee,
          SUBSCRIPTION_ACTIVE, //0 = active
          RECURRENCE_TYPE_MONTH, //1 = month, 2 = days
          1, //everymonth
          moment().format("MM-YYYY")
        ];
        await courseDao.subscribeMisc(miscSubscription);
      } else {
        return res.status(404).json({ error: { message: "This student already subscribed this misc."} });
      }
    }

    //   if (checkIfStudentSubscribeTheMisc != null){
    //     const miscById = await miscDao.getMiscById(miscItem.id);
    //     const miscSubscription = {
    //       subscriptionId: checkIfStudentSubscribeTheMisc.ssmt_id,
    //       studentId: studentId,
    //       studentName: req.body.name,
    //       miscId: miscItem.id,
    //       miscName: miscById.mt_name,
    //       miscFee: miscById.mt_fee,
    //       subscriptionStatus: SUBSCRIPTION_ACTIVE, //0 = active
    //       recurrenceType: RECURRENCE_TYPE_MONTH, //1 = month, 2 = days
    //       recurrenceDuration: 1, //everymonth
    //     };
    //
    //     await courseDao.updateStudentMisc(miscSubscription);
    //   } else {
    //     const miscById = await miscDao.getMiscById(miscItem.id);
    //     const miscSubscription = [
    //       studentId,
    //       req.body.name,
    //       miscItem.id,
    //       miscById.mt_name,
    //       miscById.mt_fee,
    //       SUBSCRIPTION_ACTIVE, //0 = active
    //       RECURRENCE_TYPE_MONTH, //1 = month, 2 = days
    //       1, //everymonth
    //     ];
    //
    //     await courseDao.subscribeMisc(miscSubscription);
    //   }
    // }

    //subscribe misc course

    return res.json({ data: { message: 'Student is created' } });
  },

  cancelCourseSubscriptionById: async (req, res) => {
    const subscriptionId = req.body.id;
    const month = req.body.month;
    const subscription = await courseDao.getCourseSubscriptionById(subscriptionId);
    const effectiveDate = moment(subscription.ssct_effective_date, "MM-YYYY");
    const cancellationDate = moment(month, "MM-YYYY");
    console.log(`current effective date ${effectiveDate}, cancellation date ${cancellationDate}, 
      and subscription id is ${subscriptionId}`)
    let status = SUBSCRIPTION_CANCELLED;
    if (cancellationDate < effectiveDate){
      return res.status(404).json({ error: { message: "Invalid cancellation date"} });
    }  else if (cancellationDate > effectiveDate) {
      cancellationDate.subtract(1, 'month'); //calculate the expiry date
    } else {
      status = SUBSCRIPTION_DELETED;
    }
    await courseDao.cancelCourseSubscriptionById(subscriptionId, cancellationDate, status);
    return res.json({ data: { message: 'Subscription is successfully cancelled' } });
  },

  cancelMiscSubscriptionById: async (req, res) => {
    const subscriptionId = req.body.id;
    const month = req.body.month;
    const cancellationDate = moment(month, "MM-YYYY");
    const subscription = await courseDao.getMiscSubscriptionById(subscriptionId);
    const effectiveDate = moment(subscription.ssmt_effective_date, "MM-YYYY");
    let status = SUBSCRIPTION_CANCELLED;
    if (cancellationDate < effectiveDate){
      return res.status(404).json({ error: { message: "Invalid cancellation date"} });
    }  else if (cancellationDate > effectiveDate) {
      cancellationDate.subtract(1, 'month'); //calculate the expiry date
    } else {
      status = SUBSCRIPTION_DELETED;
    }

    await courseDao.cancelMiscSubscriptionById(subscriptionId, cancellationDate, status);
    return res.json({ data: { message: 'Subscription is successfully cancelled' } });
  },

  updateTeacherInfo: async (req, res) => {
    let teacherInfoByUuid = await teacherDao.getTeacherInfoByUuid(req.body.uuid);
    if (teacherInfoByUuid == null) {
      return res.status(404).json({ error: { message: "This teacher doesn't exist in our database"} });
    }

    const teacherId = teacherInfoByUuid.tt_id;
    await teacherDao.updateTeacherInfo(req.body);

    //update/insert tuition fees table
    const tuitionCourses = req.body.tuitionCourse;
    for (let i = 0; i < tuitionCourses.length; i++) {
      const course = tuitionCourses[i];
      // const tuitionFee = await courseDao.getTuitionFeeById(course.id);
      const tuitionFee = await courseDao.getTuitionFeeByTeacherIdAndCourseIdAndProgramLvl(teacherInfoByUuid.tt_id, course.programLvl, course.subjectId);
      const courseObj = await courseDao.getCourseById(course.subjectId);
      const teacherName = req.body.name;
      if (tuitionFee==null) {
          const tuitionFees = [
            teacherId,
            teacherName,
            courseObj.ct_id,
            courseObj.ct_subject,
            course.programLvl,
            course.fee,
            TUITION_FEE_STATUS_ACTIVE
          ];
          await teacherDao.insertTuitionFees(tuitionFees);
      } else if (course.id == null) {
        return res.status(404).json({ error: { message: "This course is already existed in our database"} });
      }


      // don't allow teacher to allow tuition fee cuz it will affect the system
      // if (tuitionFee != null){
      //   const tuitionType = courseObj.ct_subject.toLowerCase() === ITEM_TYPE_DAYCARE.toLowerCase() ? ITEM_TYPE_DAYCARE : ITEM_TYPE_TUITION;
      //   const tuitionFees = {
      //     tuitionId: tuitionFee.tft_id,
      //     teacherName,
      //     courseId: course.subjectId,
      //     courseName: courseObj.ct_subject,
      //     programLvl: course.programLvl,
      //     tuitionType,
      //     fee: course.fee,
      //   };
      //   await teacherDao.updateTuitionFees(tuitionFees);
      //   await teacherDao.updateSubscriptionInfoByTuitionId(tuitionFees);
      // } else {
      //   const tuitionFees = [
      //     teacherId,
      //     teacherName,
      //     courseObj.ct_id,
      //     courseObj.ct_subject,
      //     course.programLvl,
      //     course.fee,
      //     TUITION_FEE_STATUS_ACTIVE
      //   ];
      //   await teacherDao.insertTuitionFees(tuitionFees);
      // }
    }

    return res.json({ data: { message: 'Student is created' } });
  },

  cancelTuitionFeeById: async (req, res) => {
    const tuitionId = req.body.id;
    await teacherDao.cancelTuitionFeeById(tuitionId);
    // await courseDao.cancelCourseSubscriptionByTuitionId(tuitionId);
    return res.json({ data: { message: 'Course is successfully cancelled' } });
  },
};
