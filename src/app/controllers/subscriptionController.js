import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import config from '../../config';
import geHeader from '../../utils/geHeader';
import { sendErrorToSlackChannel } from '../../services/slack';
import { sendConfirmationEmailToLifeApplicationUser, sendConfirmationEmailToHospiCashApplicationUser, sendEmailToSMEProtectXApplicationUser, sendEmailToCProtectApplicationUser, sendEmailToBillProtectApplicationUser, sendEmailToCardProtectApplicationUser, sendEmailToProtectSuper6ApplicationUser, sendEmailToSMEProtect3In1ApplicationUser, sendEmailToSMEOwnerProtectApplicationUser } from '../../services/mail';
import { queryOrUpdateDB, insertDB, insertGEResponse, getCProtectPaymentStatusWithMsisdn, insertValidationError } from '../../services/db';
import { generateAuthToken, encryptPaymentToken, decryptPaymentToken, generateHMAC, generateApiSecretKey } from '../../utils/crypto';
import { toCent, toDollarDisplay, calTotalAmount, generateUniqueId, generateEcertId, generateTakafulEcertId, addPrefixInsuredPersonListData, convertGeErrosMessages, getInternationalNumber } from '../../utils';



export const subscriptionController = {

};