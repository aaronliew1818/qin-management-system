import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import config from '../../config';
import geHeader from '../../utils/geHeader';
import { sendErrorToSlackChannel } from '../../services/slack';;
import { queryOrUpdateDB, insertDB, insertGEResponse, getCProtectPaymentStatusWithMsisdn, insertValidationError } from '../../services/db';
import { generateAuthToken, encryptPaymentToken, decryptPaymentToken, generateHMAC, generateApiSecretKey } from '../../utils/crypto';
import { toCent, toDollarDisplay, calTotalAmount, generateUniqueId, generateEcertId, generateTakafulEcertId, addPrefixInsuredPersonListData, convertGeErrosMessages, getInternationalNumber } from '../../utils';
import { paymentDao } from './../dao/paymentDao'
import { authDao } from './../dao/authDao'
import {studentDao} from "../dao/studentDao";
import {teacherDao} from "../dao/teacherDao";
import {courseDao} from "../dao/courseDao";
import {
  PAYMENT_ITEM_TYPE_COURSE, PAYMENT_ITEM_TYPE_MISC,
  PAYMENT_TYPE_ONE_TIME_PAYMENT,
  PAYMENT_TYPE_SUBSCRIPTION,
  PAYMENT_STATUS_PAID,
  PAYMENT_STATUS_PENDING,
  PAYMENT_TYPE_TEACHER_HOUR,
  ITEM_TYPE_TUITION, PAYMENT_TYPE_TEACHER_RATIO,
  PAYMENT_HISTORY_STUDENT,
  PAYMENT_HISTORY_TEACHER,
  ROLE_ADMIN
} from "../dao/constants";
import {miscDao} from "../dao/miscDao";
import { decodeAuthToken } from '../../utils/crypto'

function isArray(what) {
  return Object.prototype.toString.call(what) === '[object Array]';
}
export const paymentController = {
  getStudentPaymentByMonthNameAndProgramLvl: async (req, res) => {
    const studentPaymentByMonthNameAndProgramLvl =
        await paymentDao.getStudentsPayment(req.query);
    return res.json(studentPaymentByMonthNameAndProgramLvl);
  },
  getStudentPaymentDetail: async (req, res) => {
    const singleStudentPaymentDetail =
        await paymentDao.getStudentPaymentDetail(req.query);
    if (!isArray(singleStudentPaymentDetail)){
      const array = [];
      array.push(singleStudentPaymentDetail);
      return res.json(array);
    }
    return res.json(singleStudentPaymentDetail);
  },

  getStudentMiscPaymentByMonthNameAndProgramLvl: async (req, res) => {
    const studentMiscPaymentByMonthNameAndProgramLvl =
        await paymentDao.getStudentsMiscPaymentByMonth(req.query);
    return res.json(studentMiscPaymentByMonthNameAndProgramLvl);
  },

  getStudentMiscPaymentDetail: async (req, res) => {
    const singleStudentPaymentDetail =
        await paymentDao.getStudentMiscPaymentDetail(req.query);
    if (!isArray(singleStudentPaymentDetail)){
      const array = [];
      array.push(singleStudentPaymentDetail);
      return res.json(array);
    }
    return res.json(singleStudentPaymentDetail);
  },


  getTeachersSalary: async (req, res) => {
    const teachersSalary =
        await paymentDao.getTeachersSalary(req.query);
    if (!isArray(teachersSalary)){
      const array = [];
      array.push(teachersSalary);
      return res.json(array);
    }
    return res.json(teachersSalary);
  },

  getTeacherSalaryDetail: async (req, res) => {
    const teacherByUuid = await teacherDao.getTeacherInfoByUuid(req.query.teacherUuid);
    const teacherSalary =
        await paymentDao.getTeacherSalaryDetail(req.query, teacherByUuid);
    const payment = await paymentDao.checkIfTeacherIsPaid(req.query.month, teacherByUuid.tt_id);
    let paymentStatus = PAYMENT_STATUS_PENDING;
    if (payment != null){
      paymentStatus = PAYMENT_STATUS_PAID;
    }

    const response = {
      id: teacherByUuid.tt_id,
      name: teacherByUuid.tt_name,
      icNo: teacherByUuid.tt_ic,
      phoneNo: teacherByUuid.tt_phone_no,
      paymentType: teacherByUuid.tt_payment_type,
      paymentRate: teacherByUuid.tt_payment_rate,
      salaryDetails: [...teacherSalary],
      paymentStatus
    }

    return res.json(response);
  },

  registerTeacher: async (req, res) =>{
    const teacherInfoByIdNo = await teacherDao.getTeacherInfoByIdNo(req.body.identification);
    if (teacherInfoByIdNo != null){
      return res.status(404).json({ error: { message: 'Teacher is already existed in our database' } });
    }
    const data = [
      req.body.name,
      req.body.dob,
      req.body.identification,
      req.body.phoneNo,
      req.body.gender,
      req.body.paymentType,
      req.body.splitRatio
    ];
    await teacherDao.insertTeacherInfo(data);
    return res.json({ data: { message: 'Teacher info saved' } });
  },

  payStudentFees: async (req, res) =>{
    for (let i = 0; i < req.body.length ; i ++){
      const paymentDetail = req.body[i];

      const subscriptionInfo = await studentDao.getStudentSubscriptionInfoById(paymentDetail.subscriptionId);
      const isPaid = await paymentDao.getSingleStudentPaymentDetail(subscriptionInfo.ssct_student_id, paymentDetail.month, paymentDetail.subscriptionId)
      if (isPaid != null){
        // console.log(isPaid);
        // await paymentDao.updatePaymentDataByPaymentId(isPaid.pst_id, paymentDetail.transactionDate, paymentDetail.paymentType)
        continue;
      }

      const tuitionFee = await courseDao.getTuitionFeeById(subscriptionInfo.ssct_item_id);
      const courseInfo = await courseDao.getCourseById(tuitionFee.tft_course_id);
      const data = [
        PAYMENT_TYPE_SUBSCRIPTION,
        paymentDetail.month,
        paymentDetail.fee,
        PAYMENT_STATUS_PAID,
        subscriptionInfo.ssct_student_id,
        subscriptionInfo.ssct_student_name,
        subscriptionInfo.ssct_teacher_id,
        subscriptionInfo.ssct_teacher_name,
        PAYMENT_ITEM_TYPE_COURSE,
        courseInfo.ct_id,
        courseInfo.ct_subject,
        paymentDetail.subscriptionId,
        paymentDetail.transactionDate,
        paymentDetail.paymentType
      ];
      await paymentDao.insertPaymentData(data);
    }

    return res.json({ data: { message: 'Payments are saved' } });
  },
  // payStudentMiscFees: async (req, res) =>{
  //   const paymentDetails = req.body.paymentDetails;
  //   const dateDifference = req.body.dateDifference;
  //   const startMonth = req.body.startMonth;
  //   const parsedStartMonth = moment(startMonth,'"MM-DD-YYYY"');
  //   for (let i = 0; i < paymentDetails.length ; i ++){
  //     const paymentDetail = paymentDetails[i];
  //     const subscriptionInfo = await miscDao.getMiscSubscriptionInfoById(paymentDetail.subscriptionId);
  //     if (subscriptionInfo == null){
  //       continue;
  //     }
  //     const miscInfo = await miscDao.getMiscById(subscriptionInfo.ssmt_item_id);
  //     // pst_item_type, pst_item_id, pst_item_name
  //       for (let j = 0; j < dateDifference ; j ++){
  //         const paymentMonth = parsedStartMonth.add(j === 0 ? 0 : 1, 'M');
  //         const data = [
  //           PAYMENT_TYPE_SUBSCRIPTION,
  //           paymentMonth.format("MM-YYYY"),
  //           miscInfo.mt_fee,
  //           PAYMENT_STATUS_PAID,
  //           subscriptionInfo.ssmt_student_id,
  //           subscriptionInfo.ssmt_student_name,
  //           PAYMENT_ITEM_TYPE_MISC,
  //           miscInfo.mt_id,
  //           miscInfo.mt_name,
  //           paymentDetail.subscriptionId
  //         ];
  //         await paymentDao.insertStudentMiscPaymentData(data);
  //       }
  //
  //   }
  //
  //   return res.json({ data: { message: 'Payments are saved' } });
  // },

  payStudentMiscFees: async (req, res) =>{
    const paymentDetails = req.body.paymentDetails;

    const studentInfo = await studentDao.getStudentInfoByUuuid(req.body.studentUuid);
    for (let i = 0; i < paymentDetails.length ; i ++){
      const paymentDetail = paymentDetails[i];
      const miscInfo = await miscDao.getMiscById(paymentDetail.id);
      if (miscInfo == null){
        continue;
      }

      const transactionDate = moment(req.body.transactionDate, "DD-MM-YYYY");
      const paymentDate = transactionDate.format("MM-YYYY")
      const data = [
        PAYMENT_TYPE_ONE_TIME_PAYMENT,
        paymentDate,
        paymentDetail.fee,
        PAYMENT_STATUS_PAID,
        studentInfo.st_id,
        studentInfo.st_name,
        PAYMENT_ITEM_TYPE_MISC,
        miscInfo.mt_id,
        miscInfo.mt_name,
        req.body.transactionDate,
        req.body.paymentType
      ];

      await paymentDao.insertStudentMiscPaymentData(data);

    }

    return res.json({ data: { message: 'Payments are saved' } });
  },

  payTeacherSalary: async (req, res) =>{
    const teacherId = req.body.id;
    const month = req.body.month;

    const teacherDetail = await teacherDao.getTeacherInfoById(teacherId);
    if (teacherDetail == null){
      return res.status(404).json({error: { message: "Teacher doesn't exist" } });
    }

    if (teacherDetail.tt_payment_type === PAYMENT_TYPE_TEACHER_HOUR && (req.body.fee <= 0 || isNaN(parseFloat(req.body.fee)))){
      return res.status(404).json({error: { message: "Please enter teacher salary for " + month } });
    }

    const payment = await paymentDao.checkIfTeacherIsPaid(month, teacherId);
    if (payment != null){
      return res.status(404).json({
        error: { message: 'Payment is already made' } });
    }
    let data;
    if (teacherDetail.tt_payment_type === PAYMENT_TYPE_TEACHER_RATIO){
      const salaryInfo = await paymentDao.getTeacherTotalSalaryById(month, teacherId);
      data = [
        PAYMENT_TYPE_ONE_TIME_PAYMENT,
        month,
        salaryInfo.totalSalary,
        PAYMENT_STATUS_PAID,
        teacherId,
        ITEM_TYPE_TUITION
      ];
    } else {
      data = [
        PAYMENT_TYPE_ONE_TIME_PAYMENT,
        month,
        req.body.fee,
        PAYMENT_STATUS_PAID,
        teacherId,
        ITEM_TYPE_TUITION
      ];
    }

    await paymentDao.insertTeacherPayment(data);

    return res.json({ data: { message: 'Payments are saved' } });
  },

  saveCourse: async (req, res) =>{
    const courseByName = await courseDao.getCourseByName(req.body.subject);
    if (courseByName != null){
      return res.status(404).json({ error: { message: 'Course is already existed in our database' } });
    }

    const data = [
      req.body.subject,
      req.body.programLvl,
      req.body.fee
    ];

    await courseDao.insertCourse(data);
    return res.json({ data: { message: 'Course is saved' } });
  },
  getAllCourses: async (req, res) =>{
    const allCourses = await courseDao.getAllCourses();
    return res.json(allCourses);
  },

  getProgramLvl: async (req, res) =>{
    const allProgramLvl = await courseDao.getProgramLvl();
    return res.json(allProgramLvl);
  },

  getAllTeachers: async (req, res) =>{
    const teachers = await teacherDao.getTeachers();
    return res.json(teachers);
  },

  getAllMisc: async (req, res) =>{
    const misc = await miscDao.getAllMisc();
    return res.json(misc);
  },

  getStudentNameById: async (req, res) =>{
    const misc = await miscDao.getAllMisc();
    return res.json(misc);
  },

  getListOfBanks: async (req, res) =>{
    const listOfBanks = await paymentDao.getListOfBanks();
    return res.json(listOfBanks);
  },

  getPaymentHistory: async (req, res) =>{
    let paymentHistory = [];
    if (req.query.type ===  PAYMENT_HISTORY_STUDENT){
      paymentHistory = await paymentDao.getStudentPaymentHistory(req.query.uuid);
    } else if (req.query.type ===  PAYMENT_HISTORY_TEACHER){
      const user = decodeAuthToken(req);
      if (user.role === ROLE_ADMIN){
        paymentHistory = await paymentDao.getTeacherPaymentHistory(req.query.uuid);
      } else {
        paymentHistory = [];
      }
    }
    return res.json(paymentHistory);
  },

  getStudentsFinanceReport: async (req, res) =>{
    const transactionDate = moment(req.query.transactionDate, "MM-YYYY");
    let year = transactionDate.format("YYYY");
    let month = transactionDate.format("M");

    let programLevel = req.query.programLvl;
    let paymentHistory = await paymentDao.getTransactionsByMonthAndProgramLevel(year, month, programLevel);
    return res.json(paymentHistory);
  },

  getMonthlyFinanceReport: async (req, res) =>{
    const transactionDate = moment(req.query.transactionDate, "MM-YYYY");
    let year = transactionDate.format("YYYY");
    let month = transactionDate.format("M");

    let programLevel = req.query.programLvl;
    let paymentHistory = await paymentDao.getTransactionsByMonth(year, month, programLevel);
    return res.json(paymentHistory);
  },

}
