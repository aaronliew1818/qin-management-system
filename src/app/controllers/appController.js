import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import config from '../../config';
import geHeader from '../../utils/geHeader';
import { sendErrorToSlackChannel } from '../../services/slack';;
import { queryOrUpdateDB, insertDB, insertGEResponse, getCProtectPaymentStatusWithMsisdn, insertValidationError } from '../../services/db';
import { generateAuthToken, encryptPaymentToken, decryptPaymentToken, generateHMAC, generateApiSecretKey } from '../../utils/crypto';
import {
  toCent,
  toDollarDisplay,
  calTotalAmount,
  generateUniqueId,
  generateEcertId,
  generateTakafulEcertId,
  addPrefixInsuredPersonListData,
  convertGeErrosMessages,
  getInternationalNumber,
  postNotification
} from '../../utils';
import { authDao } from './../dao/authDao'
import { courseDao } from './../dao/courseDao'
import { teacherDao } from "../dao/teacherDao";
import { miscDao } from "../dao/miscDao";
import {studentDao} from "../dao/studentDao";
import {announcementDao} from "../dao/announcementDao";
import admin from "firebase-admin";
import {quizDao} from "../dao/quizDao";
import * as yup from "yup";
import {notesDao} from "../dao/notesDao";
import {timetableDao} from "../dao/timetableDao";

export const appController = {
  login: async (req, res) =>{
    const body = req.body;

    const validateUsernameAndPassword = await authDao.validateUsernameAndPassword(body.username, body.password);

    if (validateUsernameAndPassword != null){
      return res.json(
      {
        token: generateAuthToken(req.session.id),
      })
    }
    return res.status(404).json({ error: { message: 'Invalid username or password'} });
  },
  getHeathCheck: async (req, res) =>{
    res.json({status: 'live'});
  },
  getClassList: async (req, res) =>{
    return res.json({"readyToDeploy": true})
  },
  registerTeacher: async (req, res) =>{
    const teacherInfoByIdNo = await teacherDao.getTeacherInfoByIdNo(req.body.identification);
    if (teacherInfoByIdNo != null){
      return res.status(404).json({ error: { message: 'Teacher is already existed in our database' } });
    }
    const data = [
      req.body.name,
      req.body.dob,
      req.body.identification,
      req.body.phoneNo,
      req.body.gender,
      req.body.paymentType,
      req.body.splitRatio
    ];
    await teacherDao.insertTeacherInfo(data);
    return res.json({ data: { message: 'Teacher info saved' } });
  },

  saveCourse: async (req, res) =>{
    const courseByName = await courseDao.getCourseByName(req.body.subject);
    if (courseByName != null){
      return res.status(404).json({ error: { message: 'Course is already existed in our database' } });
    }

    const data = [
      req.body.subject,
      req.body.programLvl,
      req.body.fee
    ];

    await courseDao.insertCourse(data);
    return res.json({ data: { message: 'Course is saved' } });
  },
  getAllCourses: async (req, res) =>{
    const allCourses = await courseDao.getAllCourses();
    return res.json(allCourses);
  },

  getProgramLvl: async (req, res) =>{
    const allProgramLvl = await courseDao.getProgramLvl();
    return res.json(allProgramLvl);
  },

  getAllTeachers: async (req, res) =>{
    const teachers = await teacherDao.getTeachers(req.query);
    return res.json(teachers);
  },

  getAllTeachersSalary: async (req, res) =>{
    const teachers = await teacherDao.getTeachers(req.query);
    return res.json(teachers);
  },

  getSingleTeacherByUuid: async (req, res) =>{
    const teacher = await teacherDao.getSingleTeacherByUuid(req.query);
    return res.json(teacher);
  },

  getAllStudents: async (req, res) =>{
    const students = await studentDao.getStudents(req.query);
    return res.json(students);
  },

  getNewSubjectEnrollmentStudents: async (req, res) =>{
    const students = await studentDao.getNewSubjectEnrollmentStudents(req.query);
    return res.json(students);
  },

  getSingleStudentByUuid: async (req, res) =>{
    const student = await studentDao.getSingleStudentByUuid(req.query);
    return res.json(student);
  },

  getAllTuitionFees: async (req, res) =>{
    const tuitionFees = await courseDao.getAllTuitionFees();
    return res.json(tuitionFees);
  },

  getAllMisc: async (req, res) =>{
    const misc = await miscDao.getAllMisc();
    return res.json(misc);
  },

  getStudentNameById: async (req, res) =>{
    const misc = await miscDao.getAllMisc();
    return res.json(misc);
  },

  getClassroom: async (req, res) =>{
    const students = await studentDao.getClassroom(req.query);
    return res.json(students);
  },

  createQuiz: async (req, res) =>{
    // teacherId: yup.number().min(1).required("teacher id is required"),
    //     courseId: yup.number().min(1).required("course id is required"),
    //     programLvl: yup.number().min(1).required("programLvl id is required"),
    const programLvl = req.body.programLvl;
    const courseId = req.body.courseId;
    let course = await courseDao.getCourseById(courseId);
    const teacherId = req.body.teacherId;
    const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(programLvl, teacherId, courseId);
    const url = req.body.url;
    const formId = getFormIdFromUrl(url);

    const data = [
      tuitionFeeObj.tft_id,
      formId,
      req.body.title,
      url,
      "ACTIVE"
    ];
    const result = await quizDao.createQuiz(data);
    const quizList = await quizDao.getAllQuiz();
    let quiz = _.find(quizList, {id: parseInt(result.insertId)});
    if (quiz) {
      postNotification("Quiz", quiz.title);
    }
    if (quiz) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_ALL";
      postNotification("Quiz", quiz.title, topic);
    }
    return res.json(quizList);
  },

  updateQuiz: async (req, res) =>{

    const programLvl = req.body.programLvl;
    const courseId = req.body.courseId;
    let course = await courseDao.getCourseById(courseId);
    const teacherId = req.body.teacherId;
    const quizId = req.body.quizId;
    const url = req.body.url;
    const formId = getFormIdFromUrl(url);
    const status = req.body.status;

    const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(programLvl, teacherId, courseId);
    const updateQuizEntity = {
      quizId,
      tuitionId : tuitionFeeObj.tft_id,
      formId,
      title: req.body.title,
      url, status}
    await quizDao.updateQuizById(updateQuizEntity);
    const quizList = await quizDao.getAllQuiz();

    // See the "Defining the message payload" section above for details
    // on how to define a message payload.
    let quiz = _.find(quizList, {id: parseInt(req.body.quizId)});
    if (quiz) {
        const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_ALL";
        postNotification("Quiz", quiz.title, topic);
    }
    return res.json(quizList);
  },

  getQuiz: async (req, res) =>{
    const quizList = await quizDao.getAllQuiz();
    return res.json(quizList);
  },

  createAnnouncement: async (req, res) =>{
    const subjectId = req.body.subjectId;
    const programLvl = req.body.programLvl;
    const classSession = req.body.classSession;
    let course = await courseDao.getCourseById(subjectId);
    const data = [
      req.body.authorName,
      programLvl ? programLvl : null,
      subjectId ? subjectId : null,
      classSession ? classSession : null,
      req.body.content,
      "ACTIVE"
    ];
    const result = await announcementDao.createAnnouncement(data);
    const announcements = await announcementDao.getAllAnnouncements();
    let announcement = _.find(announcements, {id: parseInt(result.insertId)});
    if (announcement && course && programLvl && classSession) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_" + classSession;
      postNotification("Announcement", announcement.content, topic);
    } else if (announcement && course && programLvl) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl + "_ALL";
      postNotification("Announcement", announcement.content, topic);
    } else if (!subjectId && !programLvl && !classSession) {
      postNotification("Announcement", announcement.content);
    }
    return res.json(announcements);
  },

  updateAnnouncement: async (req, res) =>{
    const subjectId = req.body.subjectId;
    const programLvl = req.body.programLvl;
    const classSession = req.body.classSession;
    let course = await courseDao.getCourseById(subjectId);
    await announcementDao.updateAnnouncement(req.body);
    const announcements = await announcementDao.getAllAnnouncements();

    // See the "Defining the message payload" section above for details
    // on how to define a message payload.
    let announcement = _.find(announcements, {id: parseInt(req.body.id)});
    if (announcement && course && programLvl && classSession) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_" + classSession;
      postNotification("Announcement", announcement.content, topic);
    } else if (announcement && course && programLvl) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl + "_ALL";
      postNotification("Announcement", announcement.content, topic);
    } else if (!subjectId && !programLvl && !classSession) {
      postNotification("Announcement", announcement.content);
    }
    return res.json(announcements);
  },

  getAnnouncements: async (req, res) =>{
    const announcements = await announcementDao.getAllAnnouncements();
    return res.json(announcements);
  },

  createNote: async (req, res) =>{
    // teacherId: yup.number().min(1).required("teacher id is required"),
    //     courseId: yup.number().min(1).required("course id is required"),
    //     programLvl: yup.number().min(1).required("programLvl id is required"),
    const programLvl = req.body.programLvl;
    const courseId = req.body.courseId;
    let course = await courseDao.getCourseById(courseId);
    const teacherId = req.body.teacherId;
    const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(programLvl, teacherId, courseId);
    const url = req.body.url;

    const data = [
      tuitionFeeObj.tft_id,
      req.body.title,
      url,
      "ACTIVE"
    ];
    const result = await notesDao.createNote(data);
    const notes = await notesDao.getAllNotes();
    let note = _.find(notes, {id: parseInt(result.insertId)});
    if (note) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_ALL";
      postNotification("Note", note.title, topic);
    }
    return res.json(notes);
  },

  updateNote: async (req, res) =>{

    const programLvl = req.body.programLvl;
    const courseId = req.body.courseId;
    let course = await courseDao.getCourseById(courseId);
    const teacherId = req.body.teacherId;
    const noteId = req.body.noteId;
    const url = req.body.url;
    const status = req.body.status;

    const tuitionFeeObj = await teacherDao.getTuitionFeeIdByProgramLvlAndTeacherIdAndCourse(programLvl, teacherId, courseId);
    const updateNoteEntity = {
      noteId,
      tuitionId : tuitionFeeObj.tft_id,
      title: req.body.title,
      url, status}
    await notesDao.updateNoteById(updateNoteEntity);
    const notes = await notesDao.getAllNotes();

    // See the "Defining the message payload" section above for details
    // on how to define a message payload.
    let note = _.find(notes, {id: parseInt(req.body.noteId)});
    if (note) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_ALL";
      postNotification("Note", note.title, topic);
    }
    return res.json(notes);
  },

  getNotes: async (req, res) =>{
    const notes = await notesDao.getAllNotes();
    return res.json(notes);
  },

  getTimetables: async (req, res) =>{
    const timetables = await timetableDao.getTimetables();
    return res.json(timetables);
  },

  updateTimetable: async (req, res) =>{
    const programLvl = req.body.programLvl;
    const subjectId = req.body.subjectId;
    let course = await courseDao.getCourseById(subjectId);
    const startTime = req.body.startTime;
    const endTime = req.body.endTime;
    const dayOfWeek = req.body.dayOfWeek;
    const timetableId = req.body.id;
    const classroom = req.body.classroom;
    const classSession = req.body.classSession;
    const status = req.body.status;
    const updateTimetableEntity = {timetableId, startTime, endTime, dayOfWeek, subjectId, subjectName: course.ct_subject, programLvl, classroom, classSession, status};
    await timetableDao.updateTimetableById(updateTimetableEntity);
    const timetables = await timetableDao.getTimetables();

    // See the "Defining the message payload" section above for details
    // on how to define a message payload.
    let timetable = _.find(timetables, {id: parseInt(timetableId)});
    if (timetable) {
      const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_" + classSession;
      postNotification("Timetable", course.ct_subject + " has been scheduled to " + translateDayOfWeekToString(dayOfWeek) + " " +
          + startTime + "-" + endTime,  topic);
    }
    return res.json(timetables);
  },

  createTimetable: async (req, res) =>{
    // teacherId: yup.number().min(1).required("teacher id is required"),
    //     courseId: yup.number().min(1).required("course id is required"),
    //     programLvl: yup.number().min(1).required("programLvl id is required"),
    const programLvl = req.body.programLvl;
    const subjectId = req.body.subjectId;
    let course = await courseDao.getCourseById(subjectId);
    const startTime = req.body.startTime;
    const endTime = req.body.endTime;
    const dayOfWeek = req.body.dayOfWeek;
    const timetableId = req.body.id;
    const classroom = req.body.classroom;
    const classSession = req.body.classSession;

    const data = [
      subjectId,
      course.ct_subject,
      programLvl,
      startTime,
      endTime,
      dayOfWeek,
      classroom,
      classSession,
      604800,
      "ACTIVE"
    ];

    await timetableDao.createTimetable(data);

    const topic = course.ct_subject.replace(" ", "_") + "_" + programLvl +  "_" + classSession;
    postNotification("Timetable", course.ct_subject + " has been updated to " + translateDayOfWeekToString(dayOfWeek) + " " + startTime + "-" + endTime, topic);

    const timetables = await timetableDao.getTimetables();
    return res.json(timetables);
  },
}

function translateDayOfWeekToString(input){
  if (input === 1) {
    return "Monday";
  } else if (input === 2) {
    return "Tuesday";
  } else if (input === 3) {
    return "Wednesday";
  } else if (input === 4) {
    return "Thursday";
  } else if (input === 5) {
    return "Friday";
  } else if (input === 6) {
    return "Saturday";
  } else if (input === 7) {
    return "Sunday";
  }
}

function getFormIdFromUrl(url){
  const path = getPathFromUrl(url);
  let array = path.split("/");
  const longestString  = array.reduce(
      function (a, b) {
        return a.length > b.length ? a : b;
      }
  );
  return longestString;
}

function getPathFromUrl(url) {
  return url.split(/[?#]/)[0];
}
