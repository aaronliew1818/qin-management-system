import { generateAuthToken } from '../../utils/crypto';
import { authDao } from './../dao/authDao'
export const authController = {
  login: async (req, res) =>{
    const body = req.body;

    const validateUsernameAndPassword = await authDao.validateUsernameAndPassword(body.username, body.password);

    if (validateUsernameAndPassword != null){
      return res.json(
      {
        token: generateAuthToken(validateUsernameAndPassword.ut_usermame, validateUsernameAndPassword.ut_role),
        role: validateUsernameAndPassword.ut_role
      })
    }
    return res.status(404).json({ error: { message: 'Invalid username or password'} });
  },
};