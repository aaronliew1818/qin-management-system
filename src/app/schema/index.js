import * as yup from 'yup';
import moment from 'moment';
import _ from 'lodash';
import config from '../../config';
import { PAYMENT_TYPE_TEACHER_RATIO, PAYMENT_TYPE_TEACHER_HOUR } from '../../app/dao/constants';

const geDateFormat = 'DD/MM/YYYY';

const personalInfoSchema = {
  name: yup.string().matches(/^[ -~]*$/, "Full name has invalid character").matches(/^([^0-9]*)$/, "Full name has number input").required('Full name is a required field'),
  phoneNumber: yup.string().matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/, "Invalid phone number").required('Mobile phone is a required field'),
  email: yup.string().email().required('Email is a required field'),
  gender: yup.mixed().oneOf(['M', 'F']).required('Gender is a required field'),
  dob: yup.mixed()
      .test('valid-date', 'Date of birth format must be DD/MM/YYYY', val => moment(val, geDateFormat).isValid() && val.replace(/[/_]/g, '').length === 8)
      .when('identificationType', (identificationType, schema) => {
        if (identificationType === 'nric') {
          return schema.test("match-date", `Date of Birth and NRIC is not matched`, function (value) {
            const nric = this.parent.identification.split("-")[0];
            let dob = value.split("/");
            dob = `${dob[2].substring(2)}${dob[1]}${dob[0]}`;
            return nric === dob;
          })
        }
      }),
  identificationType: yup.mixed().oneOf(['nric', 'passport', 'policeOrArmy']).required('Identity Type is a required field'),
  identification: yup.string().when('identificationType', (identificationType, schema) => {
      return schema.matches(/^\d{6}-\d{2}-\d{4}$/, 'NRIC formats require 000000-00-0000').required('NRIC/Passport/Police/Army No. is a required field')
  }),
};
export const loginSchema = yup.object().shape({
  username: yup.string().required('Username is a required field'),
  password: yup.string().required('Password is a required field'),
});

const basicStudentInfoSchema = {
    name: yup.string().matches(/^[ -~]*$/, "Full name has invalid character").matches(/^([^0-9]*)$/, "Full name has number input").required('Full name is a required field'),
    phoneNo: yup.string().matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/, "Invalid phone number").required('Mobile phone is a required field'),
    email: yup.string().email().nullable(),
    gender: yup.mixed().oneOf(['M', 'F']).required('Gender is a required field'),
    nric: yup.string().matches(/^\d{6}-\d{2}-\d{4}$/, 'NRIC formats require 000000-00-0000').required('NRIC No. is a required field').nullable(),
    // dob: yup.mixed().nullable()
    //     .test('valid-date', 'Date of birth format must be DD/MM/YYYY', val => moment(val, geDateFormat).isValid() && val.replace(/[/_]/g, '').length === 8),
    tuitionCourse: yup.array()
        .of(yup
            .object()
            .shape(
                {
                    programLvl: yup.number(),
                    subjectId: yup.number(),
                    teacher: yup.number(),
                    fee: yup.number(),
                }
            ))
        .required("Tuition Courses are required"),
    misc: yup.array()
        .of(yup
            .object()
            .shape(
                {
                    id: yup.string(),
                    name: yup.string(),
                    fee: yup.string(),
                }
            ))
        .required("Misc are required"),
    schedule: yup.string().required("Schedule is required"),
    programLvl: yup.number().required("Program Level is required"),
    emergencyContactName: yup.string('Emergency contact name is a required field')
                                .matches(/^[ -~]*$/, "Full name has invalid character")
                                .matches(/^([^0-9]*)$/, "Full name has number input")
                                .min("", "Emergency contact name is a required field'"),
    emergencyContactPhoneNo: yup.mixed()
                                .nullable()
                                .test(
                                    "validate-phone-no",
                                    "Invalid phone number",
                                    value => {
                                        if (value == null || value === "") {
                                            return true;
                                        } else {
                                            const regex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
                                            return String(value).match(regex)
                                        }
                                    }
                                ),

    emergencyContactRelationship: yup.string().min("", "Emergency contact relationship is required")
};

export const createAnnouncementSchema = yup.object().shape({
    authorName: yup.string().min(1).required("Author name is required"),
    content: yup.string().min(1).required("Content is required")
});

export const updateAnnouncementSchema = yup.object().shape({
    id: yup.number().min(1).required("Post id is required"),
    authorName: yup.string().min(1).required("Author name is required"),
    content: yup.string().min(1).required("Content is required")
});

export const createQuizSchema = yup.object().shape({
    teacherId: yup.number().min(1).required("teacher id is required"),
    courseId: yup.number().min(1).required("course id is required"),
    programLvl: yup.number().min(1).required("programLvl id is required"),
    title: yup.string().min(1).required("title is required"),
    url: yup.string().min(1).required("url is required")
});

export const updateQuizSchema = yup.object().shape({
    quizId: yup.number().min(1).required("quiz id is required"),
    teacherId: yup.number().min(1).required("teacher id is required"),
    courseId: yup.number().min(1).required("course id is required"),
    programLvl: yup.number().min(1).required("programLvl id is required"),
    title: yup.string().min(1).required("Title is required"),
    url: yup.string().min(1).required("Url is required"),
    status: yup.string().min(1).required("Status is required"),
});

export const createNoteSchema = yup.object().shape({
    teacherId: yup.number().min(1).required("teacher id is required"),
    courseId: yup.number().min(1).required("course id is required"),
    programLvl: yup.number().min(1).required("programLvl id is required"),
    title: yup.string().min(1).required("Author name is required"),
    url: yup.string().min(1).required("Content is required")
});

export const updateNoteSchema = yup.object().shape({
    noteId: yup.number().min(1).required("Quiz id is required"),
    teacherId: yup.number().min(1).required("teacher id is required"),
    courseId: yup.number().min(1).required("course id is required"),
    programLvl: yup.number().min(1).required("programLvl id is required"),
    title: yup.string().min(1).required("Title is required"),
    url: yup.string().min(1).required("Url is required"),
    status: yup.string().min(1).required("Status is required"),
});

export const updateTimetableSchema = yup.object().shape({
    id: yup.number().min(1).required("Timetable id is required"),
    subjectId: yup.number().min(1).required("subject id is required"),
    programLvl: yup.number().min(1).required("programLvl id is required"),
    startTime: yup.string().min(1).required("startTime is required"),
    endTime: yup.string().min(1).required("endTime is required"),
    dayOfWeek: yup.number().min(1).required("dayOfWeek is required"),
    classroom: yup.string().min(1).required("classroom is required"),
    status: yup.string().min(1).required("status is required")
});

export const createTimetableSchema = yup.object().shape({
    subjectId: yup.number().min(1).required("subject id is required"),
    programLvl: yup.number().min(1).required("programLvl id is required"),
    startTime: yup.string().min(1).required("startTime is required"),
    endTime: yup.string().min(1).required("endTime is required"),
    dayOfWeek: yup.number().min(1).required("dayOfWeek is required"),
    classroom: yup.string().min(1).required("classroom is required")
});

export const studentInfoSchema = yup.object().shape({
  ...basicStudentInfoSchema
});

export const updateStudentInfoSchema = yup.object().shape({
    uuid: yup.string(),
    ...basicStudentInfoSchema
});

export const updateStudentStatusSchema = yup.object().shape({
    uuid: yup.string(),
    status: yup.string()
});

export const cancelSubcriptionSchema = yup.object().shape({
    id: yup.number().required('Subscription id is a required field'),
    month: yup.mixed()
        .test('valid-date', 'Month format must be in MM-YYYY', val => moment(val, "MM-YYYY").isValid()),
});

export const cancelTuitionSchema = yup.object().shape({
    id: yup.number().required('Tuition id is a required field')
});


const basicTeacherInfoSchema = {
  name: yup.string().matches(/^[ -~]*$/, "Full name has invalid character").matches(/^([^0-9]*)$/, "Full name has number input").required('Full name is a required field'),
  phoneNo: yup.string().matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/, "Invalid phone number").required('Mobile phone is a required field'),
  gender: yup.mixed().oneOf(['M', 'F']).required('Gender is a required field'),
  identification: yup.string().matches(/^\d{6}-\d{2}-\d{4}$/, 'NRIC formats require 000000-00-0000').required('NRIC No. is a required field'),
  bankName: yup.string().required("Bank name is required"),
  bankAccountNo: yup.number().required("Bank account no is required"),
  paymentType: yup.string().required("Payment Type is required"),
  paymentRate: yup.number().required("Payment rate is required"),
  tuitionCourse: yup.array()
      .of(yup
          .object()
          .shape(
              {
                programLvl: yup.number(),
                subjectId: yup.number(),
              }
          ))
      .required("Tuition Courses are required"),
};

export const teacherInfoSchema = yup.object().shape({
    ...basicTeacherInfoSchema
});

export const updateTeacherInfoSchema = yup.object().shape({
    uuid: yup.string(),
    ...basicTeacherInfoSchema
});


export const studentPaymentSchema = yup.array()
        .of(yup
            .object()
            .shape(
                {
                    programLvl: yup.string(),
                    fee: yup.number(),
                    id: yup.number(),
                    studentName: yup.string(),
                    subscriptionId: yup.number(),
                    transactionDate: yup.mixed()
                        .test('valid-date', 'Date of birth format must be DD-MM-YYYY', val => moment(val, "DD-MM-YYYY").isValid()),
                }
            ))
        .required("Payment Details are required");

export const teacherPaymentSchema = yup
        .object()
        .shape(
            {
                id: yup.number(),
                month: yup.mixed()
                    .test('valid-date', 'Date of birth format must be MM-YYYY', val => moment(val, "MM-YYYY").isValid()),
                fee: yup.number().nullable()
            }
        )
    .required("Payment Details are required");

// export const studentMiscPaymentSchema = yup.object().shape(
//         {
//             dateDifference: yup.number(),
//             startMonth: yup.mixed()
//                 .test('valid-date', 'Date of birth format must be MM-YYYY', val => moment(val, "MM-YYYY").isValid()),
//             paymentDetails: yup.array().of(yup
//                 .object()
//                 .shape(
//                     {
//                         programLvl: yup.string(),
//                         fee: yup.number(),
//                         id: yup.number(),
//                         studentName: yup.string(),
//                         subscriptionId: yup.number(),
//                     }
//                 ))
//                 .required("Payment Details are required")
//         }
//     );

export const studentMiscPaymentSchema = yup.object().shape(
    {
        studentUuid: yup.string(),
        transactionDate: yup.mixed()
            .test('valid-date', 'Transaction date must be MM-YYYY', val => moment(val, "DD-MM-YYYY").isValid()),
        paymentDetails: yup.array().of(yup
            .object()
            .shape(
                {
                    id: yup.number(),
                    fee: yup.number()
                }
        ))
        .required("Payment Details are required")

        // dateDifference: yup.number(),
        // startMonth: yup.mixed()
        //     .test('valid-date', 'Date of birth format must be MM-YYYY', val => moment(val, "MM-YYYY").isValid()),
        // paymentDetails: yup.array().of(yup
        //     .object()
        //     .shape(
        //         {
        //             programLvl: yup.string(),
        //             fee: yup.number(),
        //             id: yup.number(),
        //             studentName: yup.string(),
        //             subscriptionId: yup.number(),
        //         }
        //     ))
        //     .required("Payment Details are required")
    }
);

export const courseInfoSchema = yup.object().shape({
  subjectName: yup.string().required('Subject name is a required field'),
});

export const courseSubcriptionSchema = yup.object().shape({
  studentUuid: yup.string().required('Student uuid is a required field'),
  teacherUuid: yup.string().required('Teacher uuid is a required field'),
  itemType: yup.mixed().required('Course type a required field'),
  itemFee: yup.number().required("Amount must be more than 0"),
  itemId: yup.number().required('Item id is required'),
  paid: yup.number().required('Payment is a required field'),
});

export const paymentStatusSchema = yup.object().shape({
  pToken: yup.string().required()
});

export const resendEnrolmentEmailSchema = yup.object().shape({
  id: yup.string().required(),
  type: yup.mixed().oneOf(['P', 'CP', 'BP', 'SPX', 'HC', 'S6'], 'Type must be one of the following values: P, CP, BP, SPX, HC', 'S6').required('Type is a required field'),
});

export const saveLifeApplicationSchema = yup.object().shape({
  ...personalInfoSchema,
  nric: yup.string().matches(/^\d{6}-\d{2}-\d{4}$/, 'NRIC formats require 000000-00-0000').required('NRIC is a required field'),
  dob: yup.mixed()
    .test('valid-date', 'Date of birth format must be DD/MM/YYYY', val => moment(val, geDateFormat).isValid() && val.replace(/[/_]/g, '').length === 8)
    .test(
      "age",
      `Date of birth must be ${config.ge.lifeInsure.minAge} years old to ${config.ge.lifeInsure.maxAge} years old`,
      value => {
        const age = moment().diff(moment(value, geDateFormat), 'years');
        return age >= config.ge.lifeInsure.minAge && age <= config.ge.lifeInsure.maxAge;
      }
    ).test("match-date", `Date of Birth and NRIC is not matched`, function (value) {
      const nric = this.parent.nric.split("-")[0];
      let dob = value.split("/");
      dob = `${dob[2].substring(2)}${dob[1]}${dob[0]}`;
      return nric === dob;
    }),
    
  occupation: yup.string(),
  natureOfBusiness: yup.string(),
  policyEffectiveDate: yup.mixed()
    .test('valid-date', 'Policy effective date format must be DD/MM/YYYY', val => moment(val, geDateFormat).isValid() && val.replace(/[/_]/g, '').length === 8)
    .test(
      "min-date",
      `Policy effective date must be later than ${moment().add(1, 'days').format(geDateFormat)}`,
      (value) => {
        return moment(moment(value, geDateFormat)).isSameOrAfter(moment(moment().add(1, 'days').format(geDateFormat), geDateFormat))
      }
    )
    .test(
      "max-date",
      `Policy effective date must be at earlier than ${moment().add(6, 'months').format(geDateFormat)}`,
      value => moment(moment(value, geDateFormat)).isSameOrBefore(moment(moment().add(6, 'months'), geDateFormat))
    ),  
});


export const campaignTrackerSchema = yup.object().shape({
  utmSource: yup.string().nullable(),
  utmMedium: yup.string().nullable(),
  utmCampaign: yup.string().nullable(),
  utmContent: yup.string().nullable(),
});

export const updateCProtectPaymentStatusSchema = yup.object().shape({
  applicationId: yup.string().required(),
  status: yup.boolean().required(),
  type: yup.string().required(),
});

export const renewCProtectSchema = yup.object().shape({
  transactionId: yup.string().required(),
  subscriptionId: yup.string().required(),
  receiptId: yup.string().required(),
  phoneNumber: yup.string().required()
});

export const sendCPortectChargingSMSSchema = yup.object().shape({
  applicationId: yup.string().required(),
});



export const saveBoostProtectSchema = yup.object().shape({
  ...personalInfoSchema,
  nric: yup.string().matches(/^\d{6}-\d{2}-\d{4}$/, 'NRIC formats require 000000-00-0000').required('NRIC is a required field'),
  dob: yup.mixed()
    .test('valid-date', 'Date of birth format must be DD/MM/YYYY', val => moment(val, geDateFormat).isValid() && val.replace(/[/_]/g, '').length === 8)
    .test(
      "age",
      `Date of birth must be ${config.ge.lifeInsure.minAge} years old to ${config.ge.lifeInsure.maxAge} years old`,
      value => {
        const age = moment().diff(moment(value, geDateFormat), 'years');
        return age >= config.ge.lifeInsure.minAge && age <= config.ge.lifeInsure.maxAge;
      }
    ).test("match-date", `Date of Birth and NRIC is not matched`, function (value) {
      const nric = this.parent.nric.split("-")[0];
      let dob = value.split("/");
      dob = `${dob[2].substring(2)}${dob[1]}${dob[0]}`;
      return nric === dob;
    }),
});
