read -p "Deploy to production? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    $(aws ecr get-login --no-include-email --region ap-southeast-1)
    docker build -f Dockerfile.prod -t aspirasi-insurance-api-prod .
    docker tag aspirasi-insurance-api-prod:latest 500459076343.dkr.ecr.ap-southeast-1.amazonaws.com/aspirasi-insurance-api-prod:latest
    docker push 500459076343.dkr.ecr.ap-southeast-1.amazonaws.com/aspirasi-insurance-api-prod:latest

    TASK_ID=`aws ecs list-tasks --cluster aspirasi-insurance-api-prod --desired-status RUNNING --family aspirasi-insurance-api-prod | egrep "task" | tr "/" " " | tr "[" " " |  awk '{print $2}' | sed 's/"$//'`
    TASK_REVISION=`aws ecs describe-task-definition --task-definition aspirasi-insurance-api-prod | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`
    aws ecs stop-task --cluster aspirasi-insurance-api-prod --task ${TASK_ID}
    aws ecs update-service --cluster aspirasi-insurance-api-prod --service aspirasi-insurance-api-prod --task-definition aspirasi-insurance-api-prod:${TASK_REVISION} --desired-count 1 --force-new-deployment
fi