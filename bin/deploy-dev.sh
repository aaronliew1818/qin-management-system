$(aws ecr get-login --no-include-email --region ap-southeast-1)
docker build -f Dockerfile.dev -t aspirasi-insurance-api .
docker tag aspirasi-insurance-api:latest 500459076343.dkr.ecr.ap-southeast-1.amazonaws.com/aspirasi-insurance-api:latest
docker push 500459076343.dkr.ecr.ap-southeast-1.amazonaws.com/aspirasi-insurance-api:latest

TASK_ID=`aws ecs list-tasks --cluster aspirasi-insurance-api --desired-status RUNNING --family aspirasi-insurance-api | egrep "task" | tr "/" " " | tr "[" " " |  awk '{print $2}' | sed 's/"$//'`
TASK_REVISION=`aws ecs describe-task-definition --task-definition aspirasi-insurance-api | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`
aws ecs stop-task --cluster aspirasi-insurance-api --task ${TASK_ID}
aws ecs update-service --cluster aspirasi-insurance-api --service aspirasi-insurance-api --task-definition aspirasi-insurance-api:${TASK_REVISION} --desired-count 1 --force-new-deployment
